@echo off
echo Beginning install

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT (
    echo This is a 32bit operating system

    driverquery | findstr /i WinUsb || echo Installing driver32 && Drivers\dpinst_x86.exe /Q
    echo Installing redist32
	vcredist_x86.exe /Q
)

if %OS%==64BIT (
    echo This is a 64bit operating system

    driverquery | findstr /i WinUsb || echo Installing driver64 && Drivers\dpinst_x64.exe /Q
    echo Installing redist64
	vcredist_x64.exe /Q
)
