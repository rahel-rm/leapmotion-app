reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT (
    echo This is a 32bit operating system

    tasklist | findstr /i LeapSvc.exe || LeapSvc.exe --run
)

if %OS%==64BIT (
    echo This is a 64bit operating system

    tasklist | findstr /i LeapSvc64.exe || LeapSvc64.exe --run
)

pause
