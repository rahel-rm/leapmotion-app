@echo off

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT (
	echo This is a 32bit operating system
    echo Elevating!
	elevate_x86.exe installRequirements.bat
)

if %OS%==64BIT (
	echo This is a 64bit operating system
    echo Elevating!
	elevate_x64.exe -w -c installRequirements.bat && echo Elevation successful || echo Elevation failed
)
