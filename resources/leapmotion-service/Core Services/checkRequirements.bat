@echo off

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT (
    echo This is a 32bit operating system

    driverquery | findstr /i WinUsb > NUL && set Driver=1 || set Driver=0
    LeapSvc.exe && set Redist32=1 || set Redist32=0
    set Redist64=1
)

if %OS%==64BIT (
    echo This is a 64bit operating system

    driverquery | findstr /i WinUsb > NUL && set Driver=1 || set Driver=0
    set Redist32=1
	LeapSvc64.exe && set Redist64=1 || set Redist64=0
)


if %Driver%==1 ( echo Driver found ) else ( echo Driver missing )
if %Redist64%==1 ( echo Redist64 found ) else ( echo Redist64 missing )
if %Redist32%==1 ( echo Redist32 found ) else ( echo Redist32 missing )

if %OS%==32BIT (
    if %Driver%==1 (
        if %Redist32%==1 (
            echo Nothing to install
			goto END
        )
    )

    echo Installing
)

if %OS%==64BIT (
    if %Driver%==1 (
        if %Redist64%==1 (
            if %Redist32%==1 (
                echo Nothing to install
				goto END
            )
        )
    )

    echo Installing
)

:END