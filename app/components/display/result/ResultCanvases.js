import _ from "lodash";
import React, {Component} from "react";
import PropTypes from "prop-types";
import LeapDataPlotter from "app/components/display/plotter/LeapDataPlotter";
import Loading from "app/components/display/result/Loading";
import styles from "app/components/display/result/ResultCanvases.css";

export default class ResultCanvases extends Component {
    static propTypes = {
        results: PropTypes.object.isRequired,
        test: PropTypes.string.isRequired,
    }

    constructor(props) {
        super(props);

        this.canvases = [];
        this.state = {done: false};

        setTimeout(() => {
            this.renderPlotter();
        }, 50);
    }

    render() {
        const hasExtra = ["grab", "pinch"].includes(this.props.test);
        return (
            <div>
                {!this.state.done && (<Loading>Joonistan</Loading>)}
                {this.renderCanvases(hasExtra ? 23 : 22)}
                {!this.state.done && (<Loading>Joonistan</Loading>)}
            </div>
        );
    }

    renderCanvases(n) {
        const canvasList = [];
        _.times(n, (i) => {
            canvasList.push(
                <canvas
                    style={styles.canvas}
                    key={i}
                    ref={canvas => {this.canvases[i] = canvas;}}
                />
            );
        });
        return canvasList;
    }

    renderPlotter() {
        const data = this.props.results;
        this.index = 0;

        if (!data) {
            this.setState({done: true});
            return;
        }

        this.plotterFuncs = [];

        this.props.test === "grab" && this.addToPlotList("Käsi rusikasse", "", data.raw.grabStrength);
        this.props.test === "pinch" && this.addToPlotList("Näpistamine", "", data.raw.pinchStrength);

        this.addToPlotList("Kiirus x", "mm/s", data.byJoint[0].raw.speeds.X);
        this.addToPlotList("x kiirus LeapMotioni põhjal", "mm/s", data.raw.leapVelocityX);

        this.addToPlotList("Kiirus y", "mm/s", data.byJoint[0].raw.speeds.Y);
        this.addToPlotList("y kiirus LeapMotioni põhjal", "mm/s", data.raw.leapVelocityY);

        this.addToPlotList("Kiirus z", "mm/s", data.byJoint[0].raw.speeds.Z);
        this.addToPlotList("z kiirus LeapMotioni põhjal", "mm/s", data.raw.leapVelocityZ);

        this.addToPlotList("Kiirendus x", "mm/s2", data.byJoint[0].raw.accelerations.X);
        this.addToPlotList("Kiirendus y", "mm/s2", data.byJoint[0].raw.accelerations.Y);
        this.addToPlotList("Kiirendus z", "mm/s2", data.byJoint[0].raw.accelerations.Z);

        this.addToPlotList("Tõuge x", "mm/s3", data.byJoint[0].raw.jerks.X);
        this.addToPlotList("Tõuge y", "mm/s3", data.byJoint[0].raw.jerks.Y);
        this.addToPlotList("Tõuge z", "mm/s3", data.byJoint[0].raw.jerks.Z);

        this.addToPlotList("Sõrmeotsa x (laius)", "mm", data.raw.IndexTipPositionX);
        this.addToPlotList("Sõrmeotsa y (kõrgus)", "mm", data.raw.IndexTipPositionY);
        this.addToPlotList("Sõrmeotsa z (sügavus)", "mm", data.raw.IndexTipPositionZ);

        this.addToPlotList("Sõrmeotsa Δx (laiuse muutus)", "mm", data.raw.IndexTipPositionXDelta);
        this.addToPlotList("Vahemaa muutus x", "mm", data.byJoint[0].raw.distanceDiffs.X);

        this.addToPlotList("Sõrmeotsa Δy (kõrguse muutus)", "mm", data.raw.IndexTipPositionYDelta);
        this.addToPlotList("Vahemaa muutus y", "mm", data.byJoint[0].raw.distanceDiffs.Y);

        this.addToPlotList("Sõrmeotsa Δz (sügavuse muutus)", "mm", data.raw.IndexTipPositionZDelta);
        this.addToPlotList("Vahemaa muutus z", "mm", data.byJoint[0].raw.distanceDiffs.Z, true);

        this.plotSomething(0);
    }

    addToPlotList(description, unit, data, isLast) {
        this.plotterFuncs.push((index) => this.plotField(index, description, unit, data, isLast));
    }

    plotSomething(index) {
        setTimeout(() => {
            (this.plotterFuncs.shift())(index);
        }, 10);
    }

    plotField(index, field, unit, data, last) {
        if (!this.canvases[index]) {
            return;
        }

        this.canvases[index].width = 600;
        this.canvases[index].height = 200;

        const plotter = new LeapDataPlotter({
            el: this.canvases[index],
        });

        this.enterDataRecursive({index, data, length: data.length, plotter, field, unit, last});
    }

    enterDataRecursive({index, data, length, plotter, field, unit, last}) {
        if (!data.length) {
            if (last) {
                this.setState({done: true});
            }
            else {
                this.plotSomething(index + 1);
            }

            return;
        } // nothing more left

        const dataHead = _.take(data, 100);
        const dataTail = data.length > 100 ? _.takeRight(data, data.length - 100) : [];

        dataHead.forEach((element) => {
            plotter.plot(field, element, {
                precision: 3,
                units: unit,
                length: length,
                height: 200,
            });

            plotter.update();
        });

        window.requestAnimationFrame(() => this.enterDataRecursive({index, data: dataTail, length, plotter, field, unit, last}));
        // setTimeout(() => this.enterDataRecursive({index, data: dataTail, length, plotter, field, unit, last}), 10);
    }
}
