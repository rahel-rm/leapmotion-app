import React from "react";

export default (props) => {
    return (
        <div style={({margin: "1rem 0 3rem"})}>
            {props.children}... <i className="fa fa-spinner fa-spin" />
        </div>
    );
};
