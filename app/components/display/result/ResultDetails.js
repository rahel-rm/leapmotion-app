import _ from "lodash";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {subTypes} from "app/enums";
import CycleData from "app/components/display/result/CycleData";

export default class ResultDetails extends Component {
    static propTypes = {
        joint: PropTypes.string.isRequired,
        data: PropTypes.object.isRequired,
        test: PropTypes.string.isRequired,
    }

    render() {
        return (
            <div>
                <h3>{this.translate(this.props.joint)}:</h3>
                {this.renderMeasurements()}
                {this.renderMotionMassStats()}
                <CycleData data={this.props.data.cycles} test={this.props.test} />
                <hr />
            </div>
        );
    }

    translate(english) {
        return ({
            WristPosition: "Ranne",
            IndexTipPosition: "Nimetissõrme ots",
        })[english];
    }

    renderMeasurements() {
        if (this.props.type === subTypes.SPEED) {
            return <p>10 sekundiga sai tehtud {this.props.count} korda</p>;
        }
    }

    renderMotionMassStats() {
        const motionMass = this.props.data.total;
        if (motionMass) {
            const {distance, trajectory, acceleration, jerk} = _.mapValues(motionMass, (v) => {
                return v.toPrecision(4);
            });
            return (
                [
                    ["Kaugus:", distance, "mm"],
                    ["Trajektoor:", trajectory, "mm"],
                    ["Kiirenduse mass:", acceleration, "mm/s²"],
                    ["Tõuke mass:", jerk, "mm/s³"],
                ].map(row => (
                    <div className="row" key={row[0]}>
                        <div className="cell">{row[0]}</div>
                        <div className="cell right">{row[1]}</div>
                        <div className="cell">{row[2]}</div>
                    </div>
                ))
            );
        }
    }
}
