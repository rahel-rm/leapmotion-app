import React, {Component} from "react";
import PropTypes from "prop-types";
import styles from "app/components/display/result/CycleData.css";

export default class CycleData extends Component {
    static propTypes = {
        data: PropTypes.array.isRequired,
        test: PropTypes.string.isRequired,
    }

    render() {
        if (!["PL", "PP", "sine"].includes(this.props.test)) {return false;}
        return (
            <table className={styles.wrapper}>
                <thead>
                    <tr>
                        <th>Tsükkel</th>
                        <th>Aeg (s)</th>
                        <th>Trajektoor (mm)</th>
                        <th>Kiirenduse mass (mm/s²)</th>
                        <th>Kiirendus/aeg (mm/s²/s)</th>
                        <th>Tõuke mass (mm/s³)</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.data.map(this.renderCycle)}
                </tbody>
            </table>
        );
    }

    renderCycle({total, cycleNr}, index) {
        const {time, trajectory, acceleration, jerk} = total;
        return (
            <tr key={index}>
                <td>{cycleNr}</td>
                <td>{time.toPrecision(4)}</td>
                <td>{trajectory.toPrecision(4)}</td>
                <td>{acceleration.toPrecision(4)}</td>
                <td>{(acceleration / time).toPrecision(4)}</td>
                <td>{jerk.toPrecision(4)}</td>
            </tr>
        );
    }
}
