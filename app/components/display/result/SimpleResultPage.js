import React, {Component} from "react";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import NextTestManager from "app/components/display/NextTestManager";
import * as StageActions from "app/actions/stage";
import * as SubjectActions from "app/actions/subject";
import {pages} from "app/enums";
import styles from "app/components/display/result/SimpleResultPage.css";

export class SimpleResultPage extends Component {
    static propTypes = {
        onChangeSubjectCode: PropTypes.func.isRequired,
        onGoToPage: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {secs: this.getSeconds()};
    }

    componentDidMount() {
        if (NextTestManager.hasNext()) {
            this.setTimeouts();
        }
    }

    componentWillUnmount() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    }

    render() {
        const testsLeft = NextTestManager.hasNext();
        const testsDone = NextTestManager.getTotal() - testsLeft;
        if (testsLeft) {
            return (
                <div className="result-page">
                    <h2 data-tid="header">{testsDone}. test tehtud</h2>
                    <br />
                    <p className={styles.p}>Teste veel järel: {testsLeft}</p>
                    <button
                        onClick={::this.handleContinue}
                        className={styles.button}
                    >
                        Järgmine test algab... <i className="fa fa-arrow-right" /> {this.state.secs}
                    </button>
                </div>
            );
        }

        return (
            <div className="result-page">
                <h2 data-tid="header">Valmis!</h2>
                <p className={styles.p}>Aitäh osalemast! :)</p>
                <button
                    onClick={::this.handleNewSubjectClick}
                    className={styles.button}
                >
                    <i className="fa fa-arrow-left" /> Uus osaleja
                </button>
            </div>
        );
    }

    getSeconds() {
        const testsLeft = NextTestManager.hasNext();
        const testsDone = NextTestManager.getTotal() - testsLeft;

        if (testsDone === 1) {
            return 4;
        }
        else if (testsLeft > 0) {
            return 2;
        }
    }

    setTimeouts() {
        if (this.state.secs > 0) {
            this.timer = setTimeout(() => {
                this.setState({
                    secs: this.state.secs - 1,
                });
                this.timer = undefined;
                this.setTimeouts();
            }, 1000);
        }
        else {
            this.timer = setTimeout(() => {
                this.timer = undefined;
                this.handleContinue();
            }, 1000);
        }
    }

    handleContinue() {
        this.props.onStartTest(...NextTestManager.next());
    }

    handleNewSubjectClick() {
        this.props.onGoToPage(pages.HOME);
        this.props.onChangeSubjectCode("");
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({...StageActions, ...SubjectActions}, dispatch);
}

export default connect(undefined, mapDispatchToProps)(SimpleResultPage);
