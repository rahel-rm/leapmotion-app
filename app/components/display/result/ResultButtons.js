import React, {Component} from "react";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as StageActions from "app/actions/stage";
import * as SubjectActions from "app/actions/subject";
import {pages} from "app/enums";
import styles from "app/components/pages/MainPage.css";
import {remote} from "electron";

export class Results extends Component {
    static propTypes = {
        onChangeSubjectCode: PropTypes.func.isRequired,
        onGoToPage: PropTypes.func.isRequired,
    }

    render() {
        return (
            <div data-tid="backButton">
                <button onClick={::this.handleNewSubjectClick}>
                    <i className="fa fa-arrow-left" /> Uus testitav
                </button>
                <button onClick={::this.handleSameSubjectClick}>
                    <i className="fa fa-arrow-left" /> Jätka sama testitavaga
                </button>
                <button onClick={::this.close} className={styles.close}>
                    <i className="fa fa-close" /> Sulge
                </button>
            </div>
        );
    }

    close() {
        remote.getCurrentWindow().close();
    }

    handleNewSubjectClick() {
        this.props.onGoToPage(pages.HOME);
        this.props.onChangeSubjectCode("");
    }

    handleSameSubjectClick() {
        this.props.onGoToPage(pages.MENU);
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({...StageActions, ...SubjectActions}, dispatch);
}

export default connect(undefined, mapDispatchToProps)(Results);
