import _ from "lodash";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import LeapMotion from "app/leapmotion/Controller";
import adjustCoordinates from "app/data/transform/adjustCoordinates";
import drawBox from "app/components/display/drawingBoard/drawBox";

export class Cursor extends Component {
    static propTypes = {
        saving: PropTypes.bool,
    }

    constructor(props) {
        super(props);
        this.state = {
            previous: undefined,
        };
    }

    render() {
        return (
            <div>
                <canvas ref={(canvas) => this.canvas = canvas} style={this.getStyle()} width="100%" height="100%" />
                <div style={this.getCurrentCursorStyles()} />
            </div>
        );
    }

    componentDidMount() {
        this.setupCanvas();
        const fps60 = 1000 / 60;
        this.timer = setInterval(() => {
            this.get();
        }, fps60);

        this.updateDimensions = ::this.updateDimensions;
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillReceiveProps(newProps) {
        if (!this.props.saving && newProps.saving) {
            const context = this.canvas.getContext("2d");
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }

        window.removeEventListener("resize", this.updateDimensions);
    }

    get() {
        const frame = LeapMotion.getController().frame();
        const indexFinger = _.get(frame, "hands[0].pointables[1]");
        if (indexFinger) {
            this.frameRate = _.get(frame, "data.currentFrameRate");
            const [x, y] = indexFinger.stabilizedTipPosition;
            const [roundedX, roundedY] = adjustCoordinates.toCanvas({x, y});

            if (this.state.previous) {
                this.drawLine({
                    startX: this.state.previous.x,
                    startY: this.state.previous.y,
                    endX: roundedX,
                    endY: roundedY,
                });
            }

            this.setState({
                previous: {x: roundedX, y: roundedY},
            });
        }
    }

    getStyle() {
        return {
            ...drawBox.getCanvasStyle(),
            border: "2px dashed #44af33",
        };
    }

    getCurrentCursorStyles() {
        const previous = this.state.previous;
        if (!previous) {
            return {};
        }

        const {x, y} = previous;
        const size = 16;
        return {
            position: "fixed",
            left: x - size / 2 + drawBox.getLeftMargin() + 1,
            top: y - size / 2 + drawBox.getTopMargin() + 1,
            height: size + "px",
            width: size + "px",
            borderRadius: "50%",
            backgroundColor: "red",
            zIndex: 0,
        };
    }

    setupCanvas() {
        const context = this.canvas.getContext("2d");
        this.updateDimensions();

        context.translate(0.5, 0.5);
        context.lineWidth = 2;
    }

    drawLine({startX, startY, endX, endY}) {
        const context = this.canvas.getContext("2d");
        context.beginPath();
        context.moveTo(startX, startY);
        context.lineTo(endX, endY);
        context.stroke();
    }

    updateDimensions() {
        this.canvas.width = drawBox.getCanvasWidth();
        this.canvas.height = drawBox.getCanvasHeight();

        const context = this.canvas.getContext("2d");
        context.translate(0.5, 0.5);
        context.lineWidth = 2;
    }
}

function mapStateToProps(state) {
    return {
        saving: state.stage.saving,
    };
}

export default connect(mapStateToProps)(Cursor);
