import React, {Component} from "react";
import PropTypes from "prop-types";
import CircularProgressbar from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

export default class StayCircle extends Component {
    static propTypes = {
        progress: PropTypes.number.isRequired,
        size: PropTypes.number.isRequired,
        left: PropTypes.number.isRequired,
        top: PropTypes.number.isRequired,
    }

    static defaultProps = (() => {
        return {
            progress: 0,
            size: 150,
        };
    })();

    render() {
        return (
            <div style={this.getStyle()}>
                <CircularProgressbar
                    percentage={this.props.progress * 100}
                    background={true}
                    backgroundPadding={1}
                    textForPercentage={() => this.props.children}
                    styles={{
                        path: {stroke: "black"},
                        trail: {stroke: "white"},
                        text: {fill: "black", textAnchor: "left"},
                        background: {fill: "lightblue"},
                    }}
                />
            </div>
        );
    }

    getStyle() {
        return {
            position: "fixed",
            width: this.props.size + "px",
            height: this.props.size + "px",
            left: this.props.left,
            top: this.props.top,
            zIndex: -1,
        };
    }
}
