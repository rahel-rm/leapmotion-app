import doc from "app/components/display/drawingBoard/doc";

const getPageWidth = () => doc.getWidth();
const getPageHeight = () => doc.getHeight();

const getCanvasWidth = () => 1400;
const getCanvasHeight = () => 600;

const getTopMargin = () => getPageHeight() / 2 - getCanvasHeight() / 2;
const getLeftMargin = () => getPageWidth() / 2 - getCanvasWidth() / 2;

const getLeftToCenterThingToPage = (size) => (getPageWidth() - size) / 2;
const getTopToCenterThingToPage = (size) => (getPageHeight() - size) / 2;

const getCanvasStyle = () => ({
    position: "fixed",
    top: getTopMargin(),
    left: getLeftMargin(),
    zIndex: -1,
});

export default {
    getCanvasStyle,

    getTopMargin,
    getLeftMargin,

    getPageHeight,
    getPageWidth,

    getCanvasHeight,
    getCanvasWidth,

    getLeftToCenterThingToPage,
    getTopToCenterThingToPage,
};
