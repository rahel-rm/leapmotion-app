import PathBuffer from "app/data/motionMass/PathBuffer";

export default class CanvasHelper {
    constructor({context, height, x, y, save}) {
        this.context = context;
        this.height = height;
        this.x = x;
        this.y = y;

        this.save = save;

        if (this.save) {
            PathBuffer.reset();
            PathBuffer.push({x, y});
        }
    }

    setX(x) {
        this.x = x;
        return this;
    }

    getX() {
        return this.x;
    }

    updateStrokeStyle() {
        this.context.strokeStyle = "rgba(0, 0, 0, 0.5)";
        this.context.translate(0.5, 0.5);
        this.context.lineWidth = 10;
    }

    drawP() {
        this.start(this.x, this.y)
            .line(0, -this.height)
            .line(this.height, 0)
            .line(0, this.height);

        this.x += this.height;
        return this;
    }

    drawL() {
        this.start(this.x, this.y)
            .line(this.height / 2, -this.height)
            .line(this.height / 2, this.height);

        this.x += this.height;
        return this;
    }

    drawJoin() {
        this.start(this.x, this.y)
            .line(this.height, 0);

        this.x += this.height;
        return this;
    }

    drawSine() {
        this.start(this.x, this.y)
            .bezierSine(this.height, -this.height)
            .bezierSine(this.height, this.height);

        this.x += 2 * this.height;
        return this;
    }

    drawSpiral(rounds) {
        const space = this.height / Math.log(rounds * 2 - 1) / 26;
        for (let i = 0; i < rounds * 60 + 9; i++) {
            const angle = 0.1 * i;
            const x = this.x + space * (1 + angle) * Math.cos(angle);
            const y = this.y + space * (1 + angle) * Math.sin(angle);
            this.context.lineTo(x, y);
        }
    }

    drawLine(fromX, fromY, toX, toY) {
        this.context.moveTo(fromX, fromY);
        this.context.lineTo(toX, toY);

        if (this.save) {
            PathBuffer.push({x: toX, y: toY});
        }
    }

    drawBezier(x, y, cp1x, cp1y, cp2x, cp2y, toX, toY) {
        this.context.moveTo(x, y);
        this.context.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, toX, toY);

        if (this.save) {
            PathBuffer.push({
                bezier: true,
                cp1x, cp1y, cp2x, cp2y, toX, toY,
            });
        }
    }

    start(x, y) {
        return this.recursiveLine(x, y);
    }

    recursiveLine(x, y) {
        return {
            line: (relX, relY) => {
                const toX = x + relX;
                const toY = y + relY;

                this.drawLine(x, y, toX, toY);
                return this.recursiveLine(toX, toY);
            },

            bezierSine: (relX, relY) => {
                const magic = 0.3642124232;

                const cp1x = x + relX * magic;
                const cp2x = x + relX * (1 - magic);
                const toX = x + relX;

                const cp1y = y;
                const cp2y = y + relY;
                const toY = y + relY;

                this.drawBezier(x, y, cp1x, cp1y, cp2x, cp2y, toX, toY);

                return this.recursiveLine(toX, toY);
            },

        };
    }
}
