export default {
    getWidth: () => document.body.clientWidth,
    getHeight: () => document.body.clientHeight,
};
