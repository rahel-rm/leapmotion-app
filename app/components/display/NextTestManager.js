import {subTypes} from "app/enums";

class FixedProcessManager {
    constructor() {
        this.reset();
    }

    next() {
        return this.tests.shift();
    }

    hasNext() {
        return this.tests.length;
    }

    reset() {
        this.tests = getTests();
    }

    getTotal() {
        return getTests().length;
    }
}

export default new FixedProcessManager();

function getTests() {
    return [
        ["PL", subTypes.VISIBLE],
        ["PL", subTypes.VISIBLE],
        ["PL", subTypes.VISIBLE],
        ["PP", subTypes.VISIBLE],
        ["PP", subTypes.VISIBLE],
        ["PP", subTypes.VISIBLE],
        ["sine", subTypes.VISIBLE],
        ["sine", subTypes.VISIBLE],
        ["sine", subTypes.VISIBLE],
        ["PL", subTypes.CONTINUE],
        ["PL", subTypes.CONTINUE],
        ["PL", subTypes.CONTINUE],
    ];
}
