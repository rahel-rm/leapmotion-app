import TimeSeries from "app/components/display/plotter/TimeSeries";

const colors = ["#900", "#090", "#009", "#990", "#909", "#099"];

export default class LeapDataPlotter {
    constructor(options = {}) {
        this.options = options;
        this.seriesHash = {};
        this.series = [];
        this.colorIndex = 0;
        this.init(options.el);
    }

    init(el) {
        let canvas;
        if (el) {
            canvas = el;
        }
        else {
            canvas = document.createElement("canvas");
            canvas.className = "leap-data-plotter";
            document.body.appendChild(canvas);
        }

        this.canvas = canvas;
        this.context = canvas.getContext("2d");

        this.rescale();
    }

    // this method must be called any time the canvas changes size.
    rescale() {
        const styles = getComputedStyle(this.canvas);
        const windowWidth = parseInt(styles.width, 10);
        const windowHeight = parseInt(styles.height, 10);
        this.width = windowWidth;
        this.height = windowHeight;

        const devicePixelRatio = window.devicePixelRatio || 1;
        const backingStoreRatio = this.context.webkitBackingStorePixelRatio ||
                            this.context.mozBackingStorePixelRatio ||
                            this.context.msBackingStorePixelRatio ||
                            this.context.oBackingStorePixelRatio ||
                            this.context.backingStorePixelRatio || 1;

        const ratio = devicePixelRatio / backingStoreRatio;
        if (devicePixelRatio !== backingStoreRatio) {
            const oldWidth = this.canvas.width;
            const oldHeight = this.canvas.height;

            this.canvas.width = oldWidth * ratio;
            this.canvas.height = oldHeight * ratio;

            this.canvas.style.width = oldWidth + "px";
            this.canvas.style.height = oldHeight + "px";

            this.context.scale(ratio, ratio);
        }

        this.clear();
        this.draw();
    }

    // pushes a data point on to the plot
    // data can either be a number
    // or an array [x,y,z], which will be plotted in three graphs.
    // options:
    // - y: the graph index on which to plot this datapoint
    // - color: hex code
    // - name: name of the plot
    // - precision: how many decimals to show (for max, min, current value)
    plot(id, data, opts) {
        //    console.assert(!isNaN(data), "No plotting data received");

        opts || (opts = {});

        if (data.length) {
            for (let i = 0, c = 120; i < data.length; i++, c = ++c > 122 ? 97 : c) {
                this.getTimeSeries( id + "." + String.fromCharCode(c), opts )
                    .push( data[i], {pointColor: opts.pointColor} );
            }
        }
        else {
            this.getTimeSeries(id, opts)
                .push(data, {pointColor: opts.pointColor});
        }
    }

    getTimeSeries(id, opts) {
        let ts = this.seriesHash[id];

        if (!ts) {
            const defaultOpts = this.getOptions(id);
            const options = Object.assign(defaultOpts, opts);

            ts = new TimeSeries(options);
            this.series.push(ts);
            this.seriesHash[id] = ts;
        }

        return ts;
    }

    getOptions(name) {
        const c = this.colorIndex;
        this.colorIndex = (this.colorIndex + 1) % colors.length;
        const len = this.series.length;
        const y = len ? this.series[len - 1].y + 50 : 0;
        return {
            y: y,
            width: this.width,
            color: colors[c],
            name: name,
        };
    }

    clear() {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    draw() {
        const context = this.context;
        this.series.forEach(function(s) {
            s.draw(context);
        });
    }

    update() {
        this.clear();
        this.draw();
    }
}
