export default class TimeSeries {
    constructor(opts) {
        opts = opts || {};
        this.x = opts.x || 0;
        this.y = opts.y || 0;
        this.precision = opts.precision || 5;
        this.units = opts.units || "";
        this.width = opts.width || 1000;
        this.height = opts.height || 50;
        this.length = opts.length || 600;
        this.color = opts.color || "#000";
        this.name = opts.name || "";
        this.frameHandler = opts.frameHandler;

        this.max = -Infinity;
        this.min = Infinity;
        this.data = [];
        this.pointColors = [];
    }

    push(value, opts) {
        this.data.push(value);

        if (this.data.length >= this.length) {
            this.data.shift();
        }

        if (opts && opts.pointColor) {
            this.pointColors.push(opts.pointColor);

            // note: this can get out of sync if a point color is not set for every point.
            if (this.pointColors.length >= this.length) {
                this.pointColors.shift();
            }
        }

        return this;
    }

    draw(context) {
        const self = this;
        const xScale = (this.width - 10) / (this.length - 1);
        const yScale = -(this.height - 10) / (this.max - this.min);

        const padding = 5;
        const top = (this.max - this.min) * yScale + 10;

        context.save();
        context.strokeRect(this.x, this.y, this.width, this.height);
        context.translate(this.x, this.y + this.height - padding);
        context.strokeStyle = this.color;

        context.beginPath();

        let max = -Infinity;
        let min = Infinity;
        this.data.forEach(function(d, i) {
            if (d > max) {max = d;}
            if (d < min) {min = d;}

            if (isNaN(d)) {
                context.stroke();
                context.beginPath();
            }
            else {
                context.lineTo(i * xScale, (d - self.min) * yScale);
                if (self.pointColors[i] && (self.pointColors[i] != self.pointColors[i - 1]) ) {
                    context.stroke();
                    context.strokeStyle = self.pointColors[i];
                    context.beginPath();
                    context.lineTo(i * xScale, (d - self.min) * yScale);
                }
            }
        });
        context.stroke();

        // draw labels
        context.fillText( this.name, padding, top);
        context.fillText( this.data[this.data.length - 1].toPrecision(this.precision) + this.units, padding, 0 );

        context.textAlign = "end";
        context.fillText( this.min.toPrecision(this.precision) + this.units, this.width - padding, 0 );
        context.fillText( this.max.toPrecision(this.precision) + this.units, this.width - padding, top );
        context.textAlign = "left";
        // end draw labels

        context.restore();
        this.min = min;
        this.max = max;
    }
}
