import _ from "lodash";
import path from "path";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import plugins from "leapjs-plugins"; // eslint-disable-line no-unused-vars
import LeapMotionFileWriter from "app/data/storage/LeapMotionFileWriter";
import MotionMass from "app/data/motionMass/MotionMass";
import FrameRate from "app/components/display/FrameRate";
import LeapMotion from "app/leapmotion/Controller";
import {pages} from "app/enums";
import styles from "app/components/display/Visualizer.css";
import winCmd from "app/leapmotion/winCmd";
import * as StageActions from "app/actions/stage";

export class Visualizer extends Component {
    static propTypes = {
        isTestPage: PropTypes.bool,
        test: PropTypes.string,
    }

    render() {
        return (
            <div>
                {this.props.isTestPage && <FrameRate />}
                <div className={!this.showHand() ? styles.hide : ""}>
                    <div
                        className={styles.scene}
                        ref={div => {
                            this.div = div;
                        }}
                    />
                    <div
                        style={({position: "fixed", bottom: "3em"})}
                        className={styles.overlay}
                        ref={overlay => {
                            this.overlay = overlay;
                        }}
                    />
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.runPlugins();
    }

    componentWillReceiveProps(nextProps) {
        const currentPageIsTest = this.props.isTestPage;
        const nextPageIsTest = nextProps.isTestPage;

        if (currentPageIsTest && !nextPageIsTest) {
            this.leavingTest();
        }
        else if (!currentPageIsTest && nextPageIsTest) {
            this.enteringTest(nextProps.test);
        }
    }

    showHand(test) {
        const isRecordingTest = this.showRecording(test || this.props.test);
        return this.props.isTestPage && isRecordingTest;
    }

    showRecording(test) {
        return ["grab", "pinch"].includes(test);
    }

    enteringTest(test) {
        LeapMotion.connect();
        if (this.showRecording(test)) {
            this.playRecording(test);
        }
        this.writer = new LeapMotionFileWriter(LeapMotion.getController());
    }

    leavingTest() {
        if (this.writer) {
            this.writer.stop();

            const data = this.writer.getBuffer().get();
            const joints = [
                "IndexTipPosition",
                "WristPosition",
            ];
            this.props.onSetResultData(new MotionMass(data, joints));
        }

        const player = this.getPlayer();
        try {
            player && player.stop(); // Has to happen before disconnect. Otherwise error.
        }
        catch (e) {} // eslint-disable-line no-empty
        LeapMotion.disconnect();
    }

    playRecording(name) {
        const player = this.getPlayer();
        const cwd = winCmd.getCwd();
        player && player.setRecording({
            url: path.join(cwd, "recordings", name + ".json.lz"),
        });
    }

    getPlayer() {
        return _.get(LeapMotion.getController(), ["plugins", "playback", "player"]);
    }

    runPlugins() {
        const controller = LeapMotion.getController();
        controller.use("boneHand", {
            targetEl: this.div,
            arm: false,
        });

        controller.use("playback", {
            requiredProtocolVersion: 6,
            overlay: this.overlay,
            resumeOnHandLost: false,
            timeBetweenLoops: 100,
        });
    }
}

function mapStateToProps(state) {
    const isTestPage = () => state.stage.current === pages.TEST;
    return {
        isTestPage: isTestPage(),
        test: state.stage.test,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Visualizer);
