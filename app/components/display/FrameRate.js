import React, {Component} from "react";
import LeapMotion from "app/leapmotion/Controller";
import styles from "app/components/display/FrameRate.css";

export default class FrameRate extends Component {
    render() {
        this.timer = setTimeout(() => {this.forceUpdate();}, 250);
        return (
            <div className={styles.frameRate}>
                {this.renderFrameRate()}
            </div>
        );
    }

    componentWillUnmount() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    }

    renderFrameRate() {
        const value = this.getFrameRate();
        if (value) {
            return `Kaadrisagedus: ${Math.round(value)}`;
        }
        else {
            return "Ühendamata";
        }
    }

    getFrameRate() {
        const controller = LeapMotion.getController();
        if (controller) {
            const frame = controller.frame();
            if (LeapMotion.isFrameStreamed(frame)) {
                return frame.data.currentFrameRate;
            }
        }
    }
}
