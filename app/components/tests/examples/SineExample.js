import _ from "lodash";
import React, {Component} from "react";
import LinearExample from "app/components/tests/examples/general/LinearExample";
import CanvasHelper from "app/components/display/drawingBoard/CanvasHelper";

export default class SineExample extends Component {
    render() {
        return (
            <LinearExample
                height={this.props.height}
                width={this.props.height * 2}
                drawExample={this.drawExample}
                position={this.props.position || "inline"}
                times={this.props.times}
            />
        );
    }

    drawExample({context, height, x, y, times, save}) {
        const canvasHelper = new CanvasHelper({context, height, x, y, save});

        _.times(times).reduce((sum) => {
            return canvasHelper
                .setX(sum)
                .drawSine()
                .getX();
        }, x);
    }
}
