import React, {Component} from "react";
import SmallExampleDetails from "app/components/tests/examples/general/SmallExampleDetails";
import BigExampleDetails from "app/components/tests/examples/general/BigExampleDetails";
import Example from "app/components/tests/examples/general/Example";
import linearDetector from "app/data/cycles/LinearDetector";
import drawBox from "app/components/display/drawingBoard/drawBox";

export default class LinearExample extends Component {
    constructor(props) {
        super(props);

        const cfg = {
            height: this.props.height,
            width: this.props.width,
        };

        const detailClass = props.position === "inline" ? SmallExampleDetails : BigExampleDetails;

        this.details = new detailClass(cfg);

        if (props.position !== "inline") {
            linearDetector.setStartX(this.details.getPadding() + drawBox.getLeftMargin());
            linearDetector.setPeriodWidth(this.props.width);
            linearDetector.save();
        }
    }

    render() {
        return (
            <Example
                getCanvasWidth={this.details.getCanvasWidth.bind(this.details)}
                getCanvasHeight={this.details.getCanvasHeight.bind(this.details)}
                height={this.props.height}
                drawExample={this.props.drawExample}
                times={this.props.times || this.details.getTimes()}
                getX={this.details.getPadding}
                getY={this.details.getY}
                padding={this.details.getPadding()}
                getStyle={this.details.getStyle}
                save={this.props.position !== "inline"}
            />
        );
    }
}
