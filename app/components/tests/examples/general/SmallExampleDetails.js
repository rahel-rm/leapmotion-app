export default class SmallExampleDetails {
    constructor({height, width}) {
        this.height = height;
        this.width = width;
    }

    getCanvasWidth() {
        return this.width * 2 + this.getPadding() * 2;
    }

    getCanvasHeight() {
        return this.height;
    }

    getY() {
        return this.height;
    }

    getPadding() {
        return 5;
    }

    getTimes() {
        return 2;
    }

    getStyle() {
        return {};
    }
}
