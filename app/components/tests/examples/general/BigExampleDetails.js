import drawBox from "app/components/display/drawingBoard/drawBox";

export default class BigExampleDetails {
    constructor({height, width}) {
        this.height = height;
        this.width = width;
    }

    getCanvasWidth() {
        return drawBox.getCanvasWidth();
    }

    getCanvasHeight() {
        return drawBox.getCanvasHeight();
    }

    getY() {
        return (drawBox.getCanvasHeight() + this.height) / 2;
    }

    getPadding() {
        return 100;
    }

    getTimes() {
        return 4;
    }

    getStyle() {
        return drawBox.getCanvasStyle();
    }
}
