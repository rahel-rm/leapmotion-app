import React, {Component} from "react";
import Example from "app/components/tests/examples/general/Example";
import drawBox from "app/components/display/drawingBoard/drawBox";

export default class RadialExample extends Component {
    constructor(props) {
        super(props);

        const isInline = this.props.position === "inline";
        this.details = isInline ? this.getSmallDetails() : this.getBigDetails();
    }

    render() {
        return (
            <Example
                getCanvasWidth={this.details.getCanvasWidth}
                getCanvasHeight={this.details.getCanvasHeight}
                height={this.props.height}
                drawExample={this.props.drawExample}
                times={this.props.times}
                getX={this.details.getX}
                getY={this.details.getY}
                padding={0}
                getStyle={this.details.getCanvasStyle}
            />
        );
    }

    getSmallDetails() {
        return {
            getX: () => this.props.height / 2,
            getY: () => this.props.height / 2,
            getCanvasWidth: () => this.props.height,
            getCanvasHeight: () => this.props.height,
            getCanvasStyle: () => ({}),
        };
    }

    getBigDetails() {
        return {
            getX: () => drawBox.getCanvasWidth() / 2,
            getY: () => drawBox.getCanvasHeight() / 2,
            getCanvasWidth: drawBox.getCanvasWidth,
            getCanvasHeight: drawBox.getCanvasHeight,
            getCanvasStyle: drawBox.getCanvasStyle,
        };
    }
}
