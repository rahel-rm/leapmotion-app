import React, {Component} from "react";
import PropTypes from "prop-types";

export default class Example extends Component {
    static propTypes = {
        height: PropTypes.number.isRequired,
        times: PropTypes.number.isRequired,
        padding: PropTypes.number.isRequired,

        getX: PropTypes.func.isRequired,
        getY: PropTypes.func.isRequired,

        getCanvasWidth: PropTypes.func.isRequired,
        getCanvasHeight: PropTypes.func.isRequired,
        drawExample: PropTypes.func.isRequired,
        getStyle: PropTypes.func.isRequired,
    }

    render() {
        return (
            <canvas
                ref={(canvas) => this.canvas = canvas}
                style={this.props.getStyle()}
            />
        );
    }

    componentDidMount() {
        this.updateDrawing = ::this.updateDrawing;
        window.addEventListener("resize", this.updateDrawing);

        this.updateDrawing();
    }

    updateDrawing() {
        if (!this.canvas) {
            return;
        }

        this.canvas.width = this.props.getCanvasWidth();
        this.canvas.height = this.props.getCanvasHeight();

        this.context = this.canvas.getContext("2d");
        this.context.beginPath();
        this.props.drawExample({
            context: this.context,
            height: this.props.height,
            times: this.props.times,
            x: this.props.getX(),
            y: this.props.getY(),
            padding: this.props.padding,
            save: this.props.save,
        });
        this.context.stroke();
    }
}
