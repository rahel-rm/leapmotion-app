import React, {Component} from "react";
import RadialExample from "app/components/tests/examples/general/RadialExample";
import CanvasHelper from "app/components/display/drawingBoard/CanvasHelper";

export default class SpiralExample extends Component {
    render() {
        return (
            <RadialExample
                height={this.props.height}
                width={this.props.height}
                drawExample={this.drawExample}
                position={this.props.position || "inline"}
                times={this.props.times}
            />
        );
    }

    drawExample({context, height, times, x, y}) {
        const canvasHelper = new CanvasHelper({context, height, x, y});
        canvasHelper.setX(x).drawSpiral(times);
    }
}
