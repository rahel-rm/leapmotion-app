import React from "react";
import Cursor from "app/components/display/drawingBoard/Cursor";
import PpExample from "app/components/tests/examples/PpExample";
import StartArea from "app/components/goalDetectors/StartArea";
import EndArea from "app/components/goalDetectors/EndArea";

export default (props) => {
    let guide = "Joonista näpuotsaga tempokalt mööda trajektoori.";
    if (props.exampleTimes === 1) {
        guide += " Jätka mustrit.";
    }

    return (
        <div>
            <h2 data-tid="header">Luria <PpExample height={30} /></h2>
            <p>{guide}</p>
            <Cursor />
            {props.exampleVisible && <PpExample position="fixed" height={150} times={props.exampleTimes} />}
            <StartArea xOffset={50} yOffset={70} />
            <EndArea xOffset={-45} yOffset={70} />
        </div>
    );
};
