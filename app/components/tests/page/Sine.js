import React from "react";
import Cursor from "app/components/display/drawingBoard/Cursor";
import SineExample from "app/components/tests/examples/SineExample";
import StartArea from "app/components/goalDetectors/StartArea";
import EndArea from "app/components/goalDetectors/EndArea";

export default (props) => {
    let guide = "Joonista näpuotsaga tempokalt mööda trajektoori.";
    if (props.exampleTimes === 1) {
        guide += " Jätka mustrit.";
    }

    return (
        <div>
            <h2 data-tid="header">Luria <SineExample height={30} /></h2>
            <p>{guide}</p>
            <Cursor />
            {props.exampleVisible && <SineExample position="fixed" height={150} times={props.exampleTimes} />}
            <StartArea xOffset={45} yOffset={70} />
            <EndArea xOffset={-45} yOffset={70} />
        </div>
    );
};
