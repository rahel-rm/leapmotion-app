import React, {Component} from "react";
import PropTypes from "prop-types";
import Counter from "app/components/goalDetectors/Counter";
import Stopwatch from "app/components/goalDetectors/Stopwatch";

export default class PinchTest extends Component {
    static propTypes = {
        hasTimeLimit: PropTypes.bool,
    }

    render() {
        return (
            <div>
                <h2 data-tid="header">Näpistus</h2>
                <p>{this.getDescription()}</p>
                <Counter field="pinchStrength" {...this.getCounterGoal()} sensitivity={3} />
                <Stopwatch {...this.getStopwatchGoal()} />
            </div>
        );
    }

    getDescription() {
        if (this.props.hasTimeLimit) {
            return "Näpista nii kiiresti kui saad, 10 korda.";
        }
        else {
            return "Näpista nii palju kordi kui saad, 10 sekundi jooksul.";
        }
    }

    getCounterGoal() {
        if (this.props.hasTimeLimit) {
            return {untilCount: 10};
        }
    }

    getStopwatchGoal() {
        if (!this.props.hasTimeLimit) {
            return {untilTime: 10};
        }
    }
}
