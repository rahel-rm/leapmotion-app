import React from "react";
import Cursor from "app/components/display/drawingBoard/Cursor";
import SpiralExample from "app/components/tests/examples/SpiralExample";
import StartArea from "app/components/goalDetectors/StartArea";
import EndArea from "app/components/goalDetectors/EndArea";
import drawBox from "app/components/display/drawingBoard/drawBox";

export default (props) => {
    const size = 100;
    const getX1 = () => drawBox.getLeftToCenterThingToPage(size);
    const getY1 = () => drawBox.getTopToCenterThingToPage(size);

    return (
        <div>
            <h2 data-tid="header">Spiraal <SpiralExample height={30} times={2} /></h2>
            <p>Joonista näpuotsaga tempokalt mööda trajektoori.</p>
            <Cursor />
            {props.exampleVisible && <SpiralExample height={600} times={3} position="fixed" />}
            <StartArea getX1={getX1} getY1={getY1} size={size} />
            <EndArea getX1={getX1} xOffset={290} />
        </div>
    );
};
