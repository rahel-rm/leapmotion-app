import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import LeapMotion from "app/leapmotion/Controller";
import {pages} from "app/enums";
import * as StageActions from "app/actions/stage";

export class Stopwatch extends Component {
    static propTypes = {
        startTime: PropTypes.number,
        untilTime: PropTypes.number,

        onStartTestTimer: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {seconds: 0};
    }

    render() {
        return (
            <div>
                {this.state.seconds} sekundit algusest
                <br />
                {this.renderTimeUntil()}
            </div>
        );
    }

    renderTimeUntil() {
        if (this.props.untilTime) {
            return `${this.props.untilTime - this.state.seconds} sekundit lõpuni`;
        }
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.get();
        }, 500);

        this.handFoundListener = () => {
            this.props.onStartTestTimer();
        };
        LeapMotion.getController().on("playback.userTakeControl", this.handFoundListener);
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }

        LeapMotion.getController().removeListener("playback.userTakeControl", this.handFoundListener);
    }

    get() {
        if (this.props.startTime) {
            const now = new Date().getTime();
            const seconds = Math.round((now - this.props.startTime) / 1000);
            this.setState({seconds});

            if (this.props.untilTime && this.state.seconds >= this.props.untilTime) {
                this.props.onGoToPage(pages.RESULTS);
            }
        }
    }
}

function mapStateToProps(state) {
    return {
        startTime: state.stage.startTime,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Stopwatch);
