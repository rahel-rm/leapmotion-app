import React, {Component} from "react";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {pages} from "app/enums";
import StayArea from "app/components/goalDetectors/StayArea";
import drawBox from "app/components/display/drawingBoard/drawBox";
import StayCircle from "app/components/display/drawingBoard/StayCircle";
import * as StageActions from "app/actions/stage";

export class EndArea extends Component {
    static propTypes = {
        minTime: PropTypes.number.isRequired,
        size: PropTypes.number.isRequired,

        getX1: PropTypes.func.isRequired,
        getY1: PropTypes.func.isRequired,

        xOffset: PropTypes.number.isRequired,
        yOffset: PropTypes.number.isRequired,

        onGoToPage: PropTypes.func.isRequired,
    }

    static defaultProps = (() => {
        const size = 100;
        return {
            minTime: 1000,
            size,
            getX1: () => drawBox.getPageWidth() - drawBox.getLeftMargin() - size, // same as right margin
            getY1: () => drawBox.getTopToCenterThingToPage(size),
            xOffset: 0,
            yOffset: 0,
        };
    })();

    constructor(props) {
        super(props);
        this.state = {inAreaProgress: undefined};
    }

    getX1(props) {
        return props.getX1() + props.xOffset;
    }

    getY1(props) {
        return props.getY1() + props.yOffset;
    }

    render() {
        if (!this.props.saving) {
            return false;
        }

        return (
            <StayCircle
                progress={this.state.inAreaProgress}
                left={this.state.hide ? 0 : this.getX1(this.props)}
                top={this.getY1(this.props)}
                size={this.props.size}
            >
                Lõpp
            </StayCircle>
        );
    }

    componentWillReceiveProps(newProps) {
        if (!this.props.saving && newProps.saving) {
            this.setupEndArea();
        }
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    setupEndArea() {
        this.stayArea = new StayArea({
            x1: this.getX1(this.props),
            y1: this.getY1(this.props),
            minTime: this.props.minTime,
        });

        this.timer = setInterval(() => {
            this.get();
        }, 200);
    }

    get() {
        const inAreaProgress = this.stayArea.millisInArea() / this.props.minTime;
        this.setState({inAreaProgress});

        if (this.stayArea.isInAreaLongEnough()) {
            if (!this.endDone) {
                this.endDone = 1;
            }
            else if (this.endDone === 1) {
                this.endDone = 2;
                this.props.onGoToPage(pages.RESULTS);
            }
        }
    }
}

function mapStateToProps(state) {
    return {
        saving: state.stage.saving,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EndArea);
