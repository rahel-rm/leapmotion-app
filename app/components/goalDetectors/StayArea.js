import _ from "lodash";
import AreaDetector from "app/components/goalDetectors/AreaDetector";

export default class StayArea {
    constructor(config) {
        const expected = ["x1", "y1", "minTime"];
        const actual = _.keys(config);
        if (_.difference(expected, actual).length) {
            throw `Required params missing in StayArea: \n Expected: ${expected.join(", ")}, got: ${actual.join(", ")}`;
        }
        _.assign(this, config);

        this.areaDetector = new AreaDetector(config);
    }

    millisInArea() {
        if (this.isInArea()) {
            return new Date().getTime() - this.enteredArea;
        }
        else {
            return 0;
        }
    }

    isInArea() {
        const inArea = this.areaDetector.isInArea();

        if (inArea && !this.enteredArea) {
            this.enteredArea = new Date().getTime();
        }
        if (!inArea && this.enteredArea) {
            this.enteredArea = undefined;
        }

        return inArea;
    }

    isInAreaLongEnough() {
        return this.enteredArea && (this.millisInArea() > this.minTime);
    }
}
