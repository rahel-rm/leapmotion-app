import _ from "lodash";
import LeapMotion from "app/leapmotion/Controller";

export default class PeakDetector {
    constructor(type, sensitivity) {
        this.type = type;
        this.sensitivity = sensitivity;
        this.isClosing = true; // kas -> 1
        this.maxValue = 1;
        this.minValue = 0;
        this.historyIndex = 0;

        if (sensitivity > 4 || sensitivity < 1) {
            throw Error("Sensitivity value invalid, has to be between 1 and 4 (inclusive)");
        }
    }

    handStateFromHistory() {
        const frame = LeapMotion.getController().frame();
        const hand = this.getHand(frame);
        if (!hand) {return "missing";}

        const currentFrameIndex = this.getHistory(frame).get(0).historyIdx;
        const historySamples = currentFrameIndex - this.historyIndex;
        const history = this.stateFromHistory(frame, historySamples);
        const info = this.extractInfo(history, hand);
        this.historyIndex = _.get(_.last(history), "historyIdx");
        return this.suggestState(this.sensitivity, {
            first: _.first(info),
            avg: _.mean(info),
            last: _.last(info),
            max: _.max(info),
            min: _.min(info),
        });
    }

    suggestState(sensitivity, {first, avg, last, max, min}) {
        // Sensitivity. Less sensitive means hand has to open/close more for it to count.
        // Least sensitive             1    2    3    4     Most sensitive
        // Hand open threshold (max)   0.1  0.2  0.3  0.4
        // State change minimum        0.53 0.4  0.23 0.15
        // Hand closed threshold (min) 0.9  0.8  0.7  0.6
        const HAND_OPEN_THRESHOLD = sensitivity * 0.1;
        const HAND_CLOSED_THRESHOLD = 1 - HAND_OPEN_THRESHOLD;
        const HAND_CHANGE_THRESHOLD = (HAND_CLOSED_THRESHOLD - HAND_OPEN_THRESHOLD) / 1.5;

        if (avg > HAND_CLOSED_THRESHOLD) {
            return "closed";
        }
        else if (avg < HAND_OPEN_THRESHOLD) {
            return "open";
        }
        else if (first < avg && avg < last) { // kasvab
            // return "closing";
        }
        else if (first < avg && avg > last) { // max
            if (this.isClosing && max - this.minValue > HAND_CHANGE_THRESHOLD) {
                this.maxValue = max;
                this.isClosing = false;
                return "closed";
            }
        }
        else if (first > avg && avg > last) { // kahaneb
            // return "opening";
        }
        else if (first > avg && last > avg) { // min
            if (!this.isClosing && this.maxValue - min > HAND_CHANGE_THRESHOLD) {
                this.minValue = min;
                this.isClosing = true;
                return "open";
            }
        }
    }

    getHistory(frame) {
        return _.get(frame, "controller.history");
    }

    stateFromHistory(frame, samples) {
        const history = this.getHistory(frame);
        const {size} = history;
        const sliced = _.map(Array(size > samples ? samples : size), (b, i) => history.get(i));
        return sliced.reverse();
    }

    getHand(frame) {
        const data = frame.data;
        if (_.get(data, "playback")) {return;}
        return _.get(data, "hands[0]");
    }

    extractInfo(history, {id: handId}) {
        return history
            .map((sample) => sample.hand(handId))
            .filter((hand) => hand.valid)
            .map((hand) => hand[this.type]);
    }
}
