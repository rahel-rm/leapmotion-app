import _ from "lodash";
import LeapMotion from "app/leapmotion/Controller";
import adjustCoordinates from "app/data/transform/adjustCoordinates";

export default class AreaDetector {
    constructor(config) {
        const expected = ["x1", "y1"];
        const actual = _.keys(config);
        if (_.difference(expected, actual).length) {
            throw `Required params missing in AreaDetector: \n Expected: ${expected.join(", ")}, got: ${actual.join(", ")}`;
        }
        _.assign(this, config);
    }

    isInArea() {
        const pointer = this.getLastValidPointer();
        return this.boundsContain(pointer);
    }

    getLastValidPointer() {
        const frame = LeapMotion.getController().frame();
        const coords = _.get(frame, "hands[0].pointables[1].stabilizedTipPosition") || [];
        if (!_.isEmpty(coords)) {
            const [x, y] = coords;
            const [roundedX, roundedY] = adjustCoordinates.toScreen({x, y});
            this.lastValid = {x: roundedX, y: roundedY};
            return this.lastValid;
        }
    }

    // Right side and bottom are open.
    boundsContain({x, y} = {}) {
        if (!x || !y) {return false;}
        const {x1, y1, x2, y2} = this;
        return between(x1, x, x2) && between(y1, y, y2);
    }
}

function between(start, n, end) {
    if (start > n) {
        return false;
    }

    if (typeof end !== "undefined" && end < n) {
        return false;
    }

    return true;
}
