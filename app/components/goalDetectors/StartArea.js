import React, {Component} from "react";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import StayArea from "app/components/goalDetectors/StayArea";
import drawBox from "app/components/display/drawingBoard/drawBox";
import StayCircle from "app/components/display/drawingBoard/StayCircle";
import * as StageActions from "app/actions/stage";

export class StartArea extends Component {
    static propTypes = {
        minTime: PropTypes.number.isRequired,
        size: PropTypes.number.isRequired,

        getX1: PropTypes.func.isRequired,
        getY1: PropTypes.func.isRequired,

        xOffset: PropTypes.number.isRequired,
        yOffset: PropTypes.number.isRequired,

        onGoToPage: PropTypes.func.isRequired,
    }

    static defaultProps = (() => {
        const size = 100;
        return {
            minTime: 1500,
            size,
            getX1: () => drawBox.getLeftMargin(),
            getY1: () => drawBox.getTopToCenterThingToPage(size),
            xOffset: 0,
            yOffset: 0,
        };
    })();

    constructor(props) {
        super(props);
        this.state = {inAreaProgress: undefined, hide: false, startDone: 0};
        const {minTime, size} = props;
        const [x1, y1] = [this.getX1(props), this.getY1(props)];
        const [x2, y2] = [x1 + size, y1 + size];
        this.stayArea = new StayArea({x1, y1, x2, y2, minTime});
    }

    getX1(props) {
        return props.getX1() + props.xOffset;
    }

    getY1(props) {
        return props.getY1() + props.yOffset;
    }

    render() {
        if (this.state.hide) {
            return false;
        }

        return (
            <StayCircle
                progress={this.state.inAreaProgress}
                left={this.getX1(this.props)}
                top={this.getY1(this.props)}
                size={this.props.size}
            >
                Alusta
            </StayCircle>
        );
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.get();
        }, 200);
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    get() {
        const inAreaProgress = this.stayArea.millisInArea() / this.props.minTime;
        this.setState({inAreaProgress});

        if (this.stayArea.isInAreaLongEnough()) {
            if (this.state.startDone === 0) {
                this.setState({startDone: 1});
            }
            else if (this.state.startDone === 1) {
                this.setState({hide: true, startDone: 2});
                this.props.onStartTracking();
            }
        }
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(undefined, mapDispatchToProps)(StartArea);
