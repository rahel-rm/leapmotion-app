import React, {Component} from "react";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {pages} from "app/enums";
import PeakDetector from "app/components/goalDetectors/PeakDetector";
import * as StageActions from "app/actions/stage";

export class Counter extends Component {
    static propTypes = {
        sensitivity: PropTypes.number.isRequired,
        count: PropTypes.number.isRequired,
        untilCount: PropTypes.number,

        onStartTracking: PropTypes.func.isRequired,
        onIncrementCount: PropTypes.func.isRequired,
        onGoToPage: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {lastState: undefined};
        this.detector = new PeakDetector(this.props.field, this.props.sensitivity);
    }

    render() {
        return (
            <div>
                {this.props.count} korda tehtud
            </div>
        );
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.get();
        }, 100);
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    get() {
        const state = this.detector.handStateFromHistory();
        const lastState = this.state.lastState;
        if (lastState !== state) {
            const wasClosed = state === "closed";
            // const wasClosed = state === "closing" || state === "closed";
            const isOpening = lastState === "open";
            // const isOpening = lastState === "opening" || lastState === "open";
            const prevGrab = state ? {lastState: state} : {};
            this.setState({...prevGrab});

            if (!this.startDone && isOpening) {
                this.startDone = true;
                this.props.onStartTracking();
            }

            if (wasClosed && isOpening) {
                const {count, untilCount} = this.props;
                if (untilCount && count >= untilCount) {
                    this.props.onGoToPage(pages.RESULTS);
                }
                else {
                    this.props.onIncrementCount();
                }
            }
        }
    }
}

function mapStateToProps(state) {
    return {
        count: state.stage.count,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
