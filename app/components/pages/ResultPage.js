import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import ResultCanvases from "app/components/display/result/ResultCanvases";
import ResultDetails from "app/components/display/result/ResultDetails";
import ResultButtons from "app/components/display/result/ResultButtons";
import SimpleResultPage from "app/components/display/result/SimpleResultPage";
import Loading from "app/components/display/result/Loading";
import PlExample from "app/components/tests/examples/PlExample";
import PpExample from "app/components/tests/examples/PpExample";
import SineExample from "app/components/tests/examples/SineExample";
import SpiralExample from "app/components/tests/examples/SpiralExample";

const components = {
    PL: PlExample,
    PP: PpExample,
    sine: SineExample,
    spiral: SpiralExample,
};

export class Results extends Component {
    static propTypes = {
        fixedProcess: PropTypes.bool,
        results: PropTypes.object,
        test: PropTypes.string.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {
            done: false,
            showCanvas: false,
        };
    }

    componentWillMount() {
        setTimeout(() => {
            this.props.results.calc();
            this.setState({
                done: true,
            });
        }, 0);
    }

    render() {
        if (this.props.fixedProcess) {
            return (
                <SimpleResultPage />
            );
        }
        return (
            <div className="result-page">
                <h2 data-tid="header">Valmis!</h2>
                {this.renderExample()}
                {this.renderResults()}
                <br />
                <ResultButtons />
            </div>
        );
    }

    renderExample() {
        const Cmp = components[this.props.test];
        const example = Cmp ? <Cmp position="inline" height={30} times={2} /> : "";
        return (
            <div>
                Test oli: {this.translate(this.props.test)}
                {example}
            </div>
        );
    }

    translate(test) {
        return ({
            grab: "käsi rusikasse",
            pinch: "näpistus",
            PL: "luria PL",
            PP: "luria PP",
            sine: "luria siinus",
            spiral: "spiraal",
        })[test];
    }

    renderResults() {
        if (!this.state.done) {
            return (<Loading>Arvutan</Loading>);
        }

        const data = this.props.results.getData();
        if (!data || !data.byJoint) {
            return false;
        }

        return (
            <div>
                {this.renderDetails(data.byJoint)}
                {this.renderCanvasOrButton(data)}
            </div>
        );
    }

    renderDetails(byJoint) {
        return byJoint.map((jointData, index) => (
            <ResultDetails
                key={index}
                joint={jointData.joint}
                data={jointData}
                test={this.props.test}
            />
        ));
    }

    renderCanvasOrButton(data) {
        if (this.state.showCanvas) {
            return (
                <div>
                    <ResultButtons />
                    <ResultCanvases results={data} test={this.props.test} />
                </div>
            );
        }
        else {
            return (
                <button onClick={() => this.setState({showCanvas: true})}>
                    <i className="fa fa-line-chart" style={{marginRight: "0.5rem"}} />
                  Joonista graafikud
                </button>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        test: state.stage.test,
        results: state.stage.results,
    };
}

export default connect(mapStateToProps)(Results);
