import React, {Component} from "react";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import GrabTest from "app/components/tests/page/GrabTest";
import PinchTest from "app/components/tests/page/PinchTest";
import PLTest from "app/components/tests/page/PL";
import PPTest from "app/components/tests/page/PP";
import Sine from "app/components/tests/page/Sine";
import Spiral from "app/components/tests/page/Spiral";
import BackButton from "app/components/BackButton";
import {pages, subTypes} from "app/enums";
import * as StageActions from "app/actions/stage";

export class Test extends Component {
    static propTypes = {
        fixedProcess: PropTypes.bool,
        currentTest: PropTypes.string.isRequired,
        subType: PropTypes.string.isRequired,

        onGoToPage: PropTypes.func.isRequired,
    }

    render() {
        return (
            <div className="full-height">
                {this.renderCurrentTest()}
                {this.renderButtons()}
            </div>
        );
    }

    renderButtons() {
        if (!this.props.fixedProcess) {
            return (
                <div className="buttons">
                    <BackButton onClick={::this.handleBackClick} />
                    <button onClick={::this.onDoneClick}>
                        Valmis <i className="fa fa-arrow-right" />
                    </button>
                </div>
            );
        }
    }

    renderCurrentTest() {
        const hasTimeLimit = this.props.subType === subTypes.TIMES;
        const exampleVisible = this.props.subType !== subTypes.BLIND;
        const exampleTimes = this.props.subType === subTypes.CONTINUE ? 1 : 4;

        switch (this.props.currentTest) {
            case "grab":
                return (<GrabTest hasTimeLimit={hasTimeLimit} />);
            case "pinch":
                return (<PinchTest hasTimeLimit={hasTimeLimit} />);
            case "PL":
                return (<PLTest exampleVisible={exampleVisible} exampleTimes={exampleTimes} />);
            case "PP":
                return (<PPTest exampleVisible={exampleVisible} exampleTimes={exampleTimes} />);
            case "sine":
                return (<Sine exampleVisible={exampleVisible} exampleTimes={exampleTimes} />);
            case "spiral":
                return (<Spiral exampleVisible={exampleVisible} />);
        }
    }

    onDoneClick() {
        this.props.onGoToPage(pages.RESULTS);
    }

    handleBackClick() {
        this.props.onGoToPage(pages.MENU);
    }
}

function mapStateToProps(state) {
    return {
        currentTest: state.stage.test,
        subType: state.stage.subType,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Test);
