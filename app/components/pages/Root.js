import React from "react";
import MainPage from "app/components/pages/MainPage";
import {Provider} from "react-redux";

type RootType = {
    store: {},
};

export default function Root({store}: RootType) {
    return (
        <Provider store={store}>
            <MainPage />
        </Provider>
    );
}
