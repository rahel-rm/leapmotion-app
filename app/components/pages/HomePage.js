import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {pages} from "app/enums";
import NextTestManager from "app/components/display/NextTestManager";
import linearDetector from "app/data/cycles/LinearDetector";
import * as SubjectActions from "app/actions/subject";
import * as StageAction from "app/actions/stage";
import styles from "app/components/pages/MainPage.css";

export class HomePage extends Component {
    static propTypes = {
        code: PropTypes.string,
        fixedProcess: PropTypes.bool,
        onChangeSubjectCode: PropTypes.func.isRequired,
        onGoToPage: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className={styles.middleContainer}>
                <div>
                    <form onSubmit={::this.onSubmit}>
                        {this.renderLabel()}
                        <input
                            className={styles.input}
                            autoFocus
                            required
                            onChange={::this.handleChange}
                            type="text"
                            value={this.props.code}
                        />
                        <button className={styles.continueButton} data-tid="continue">
                            Alusta <i className="fa fa-arrow-right" />
                        </button>
                        {this.renderExplanation()}
                    </form>
                </div>
            </div>
        );
    }

    renderLabel() {
        if (this.props.fixedProcess) {
            return (<h3>Ees- ja perekonnanimi:</h3>);
        }
        return (<h3>Sisesta testitava kood (kohustuslik):</h3>);
    }

    renderExplanation() {
        if (!this.props.fixedProcess) {
            return false;
        }

        const testNr = NextTestManager.getTotal();
        return (
            <p className={styles.explanation}>
                Iga osaleja jaoks on kokku {testNr} testi.
                <br />
                Sellest oleks palju abi, kui sa kõik testid lõpuni teeks.
                <br />
                Ja palun too vanema luba, muidu ei saa tulemusi kasutada.
            </p>
        );
    }

    handleChange(event) {
        this.props.onChangeSubjectCode(event.target.value);
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.props.fixedProcess) {
            NextTestManager.reset();
            this.props.onStartTest(...NextTestManager.next());
            linearDetector.reset();
        }
        else {
            this.props.onGoToPage(pages.MENU);
        }
    }
}

function mapStateToProps(state) {
    return {
        code: state.subject.code,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({...SubjectActions, ...StageAction}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
