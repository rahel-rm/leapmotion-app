import React, {Component} from "react";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import BackButton from "app/components/BackButton";
import * as StageActions from "app/actions/stage";
import {pages, subTypes} from "app/enums";
import PlExample from "app/components/tests/examples/PlExample";
import PpExample from "app/components/tests/examples/PpExample";
import SineExample from "app/components/tests/examples/SineExample";
import SpiralExample from "app/components/tests/examples/SpiralExample";
import linearDetector from "app/data/cycles/LinearDetector";

export class ChooseTestPage extends Component {
    static propTypes = {
        onStartTest: PropTypes.func.isRequired,
        onGoToPage: PropTypes.func.isRequired,
    }

    render() {
        return (
            <div className="full-height">
                <h2 data-tid="menu-header">Vali harjutus</h2>
                <div className="tests">
                    <div className="flex">
                        <section>
                            <h3>Luria <PlExample height={30} /></h3>
                            {this.renderChooseDrawTest("PL")}
                        </section>
                        <section>
                            <h3>Luria <PpExample height={30} /></h3>
                            {this.renderChooseDrawTest("PP")}
                        </section>
                        <section>
                            <h3>Luria <SineExample height={30} /></h3>
                            {this.renderChooseDrawTest("sine")}
                        </section>
                    </div>
                    <hr />
                    <div className="flex">
                        <section>
                            <h3>Käsi rusikasse</h3>
                            {this.renderChooseGestureTest("grab")}
                        </section>
                        <section>
                            <h3>Näpistus</h3>
                            {this.renderChooseGestureTest("pinch")}
                        </section>
                        <section>
                            <h3>Spiraal <SpiralExample height={30} times={2} /></h3>
                            {this.renderChooseDrawTest("spiral")}
                        </section>
                    </div>
                </div>
                <div className="buttons">
                    <BackButton onClick={::this.handleBackClick} />
                </div>
            </div>
        );
    }

    renderChooseGestureTest(type) {
        return (
            <ul>
                <li><a onClick={() => this.props.onStartTest(type, subTypes.TIMES)} href="#">10 korda</a></li>
                <li><a onClick={() => this.props.onStartTest(type, subTypes.SPEED)} href="#">10 sekundit</a></li>
            </ul>
        );
    }

    renderChooseDrawTest(type) {
        const visible = <li><a onClick={() => this.onGoToDrawTest(type, subTypes.VISIBLE)} href="#">nähtav</a></li>;
        const cont = <li><a onClick={() => this.onGoToDrawTest(type, subTypes.CONTINUE)} href="#">jätkamine</a></li>;
        const blind = <li><a onClick={() => this.onGoToDrawTest(type, subTypes.BLIND)} href="#">pimesi</a></li>;
        return (
            <ul>
                {visible}
                {type !== "spiral" ? cont : false}
                {blind}
            </ul>
        );
    }

    handleBackClick() {
        this.props.onGoToPage(pages.HOME);
    }

    onGoToDrawTest(type, subType) {
        this.props.onStartTest(type, subType);
        linearDetector.reset();
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(StageActions, dispatch);
}

export default connect(undefined, mapDispatchToProps)(ChooseTestPage);
