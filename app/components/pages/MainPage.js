import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import HomePage from "app/components/pages/HomePage";
import ChooseTestPage from "app/components/pages/ChooseTestPage";
import Visualizer from "app/components/display/Visualizer";
import ResultPage from "app/components/pages/ResultPage";
import TestPage from "app/components/pages/TestPage";
import styles from "app/components/pages/MainPage.css";

export class MainPage extends Component {
    static propTypes = {
        currentPage: PropTypes.string.isRequired,
    }

    render() {
        return (
            <div className={styles.container} data-tid="container">
                {this.renderCurrentPage()}
                <Visualizer />
            </div>
        );
    }

    renderCurrentPage() {
        const fixedProcess = true;
        switch (this.props.currentPage) {
            case "menu":
                return <ChooseTestPage />;
            case "test":
                return <TestPage fixedProcess={fixedProcess} />;
            case "results":
                return <ResultPage fixedProcess={fixedProcess} />;
            default:
                return <HomePage fixedProcess={fixedProcess} />;
        }
    }
}

function mapStateToProps(state) {
    return {
        currentPage: state.stage.current,
    };
}

export default connect(mapStateToProps)(MainPage);
