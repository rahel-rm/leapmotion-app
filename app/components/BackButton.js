import React, {Component} from "react";
import PropTypes from "prop-types";

export default class BackButton extends Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
    }

    render() {
        return (
            <button onClick={this.props.onClick}>
                <i className="fa fa-arrow-left" /> {this.props.children ? this.props.children : "Tagasi"}
            </button>
        );
    }
}
