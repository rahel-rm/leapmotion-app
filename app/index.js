import React from "react";
import {render} from "react-dom";
import {AppContainer} from "react-hot-loader";
import {store} from "app/store/store";
import Root from "app/components/pages/Root";
import "app/app.global.css";

render(
    <AppContainer>
        <Root store={store} />
    </AppContainer>,
    document.getElementById("root")
);

if (module.hot) {
    module.hot.accept("app/components/pages/Root", () => {
        const NextRoot = require("app/components/pages/Root"); // eslint-disable-line global-require
        render(
            <AppContainer>
                <NextRoot store={store} />
            </AppContainer>,
            document.getElementById("root")
        );
    });
}
