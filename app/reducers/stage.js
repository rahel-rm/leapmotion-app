import {pages} from "app/enums";
import {
    ON_GO_TO_PAGE,
    ON_START_TEST,
    ON_START_TRACKING,
    ON_INCREMENT_COUNT,
    ON_START_TEST_TIMER,
    ON_SET_RESULT_DATA,
} from "app/actions/stage";

export default function stage(state = {current: pages.HOME, test: ""}, action) {
    switch (action.type) {
        case ON_GO_TO_PAGE:
            return {
                ...state,
                current: action.nextPage,
                saving: false,
            };
        case ON_START_TEST:
            return {
                ...state,
                current: pages.TEST,
                test: action.test,
                subType: action.subType,
                count: 0,
                startTime: 0,
            };
        case ON_START_TRACKING:
            return {
                ...state,
                saving: true,
            };
        case ON_INCREMENT_COUNT:
            return {
                ...state,
                count: state.count + 1,
            };
        case ON_START_TEST_TIMER:
            return {
                ...state,
                startTime: Date.now(),
            };
        case ON_SET_RESULT_DATA:
            return {
                ...state,
                results: action.data,
            };
        default:
            return state;
    }
}
