import {ON_CHANGE_SUBJECT_CODE} from "app/actions/subject";

export default function subject(state = {code: ""}, action) {
    switch (action.type) {
        case ON_CHANGE_SUBJECT_CODE:
            return {code: action.code};
        default:
            return state;
    }
}
