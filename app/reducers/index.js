import {combineReducers} from "redux";
import subject from "app/reducers/subject";
import stage from "app/reducers/stage";

const rootReducer = combineReducers({
    subject,
    stage,
});

export default rootReducer;
