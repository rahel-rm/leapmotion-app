import _ from "lodash";

export default function arrangeDataToCycles({cycles, dataByAxis, thresholds}) {
    return cycles.map((cycle) => {
        const startIndex = thresholds[cycle.cycleNr];
        const endIndex = (cycle.cycleNr + 1) < thresholds.length ? thresholds[cycle.cycleNr + 1] : undefined;
        return {
            ...cycle,
            ...getDataForCycle(dataByAxis, startIndex, endIndex),
        };
    });
}

function getDataForCycle(data, startIndex, endIndex) {
    return _.mapValues(data, (maybeList) => {
        if (_.isArray(maybeList)) {
            return maybeList.slice(startIndex, endIndex);
        }
        else if (_.isObject(maybeList)) {
            return getDataForCycle(maybeList, startIndex, endIndex);
        }
        return maybeList;
    });
}
