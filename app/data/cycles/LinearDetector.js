import fs from "fs";
import path from "path";
import moment from "moment";
import winCmd from "app/leapmotion/winCmd";
import adjustCoordinates from "app/data/transform/adjustCoordinates";

class LinearDetector {
    constructor() {
        this.reset();
    }

    setStartX(value) {
        this.screenStartX = value;
        this.leapStartX = adjustCoordinates.screenToLeapMotion({x: value, y: 0})[0];
    }

    setPeriodWidth(value) {
        this.screenPeriodWidth = value;
        this.leapPeriodWidth = value / adjustCoordinates.getPixelScale();
    }

    getScreenCycleNr(x) {
        if (!this.screenPeriodWidth) {return 0;}
        return Math.floor((x - this.screenStartX) / this.screenPeriodWidth) + 1;
    }

    getCycleNr(x) {
        if (!this.leapPeriodWidth) {return 0;}
        return Math.floor((x - this.leapStartX) / this.leapPeriodWidth) + 1;
    }

    reset() {
        Object.assign(this, {
            screenStartX: 0,
            leapStartX: 0,
            screenPeriodWidth: 0,
            leapPeriodWidth: 0,
        });
    }

    save() {
        const time = moment().format("YYYY-MM-DD-THHmmss");
        const home = winCmd.getUserHome();
        const fileName = path.join(home, "thesis", "cycles", `cycle-${time}.txt`);
        winCmd.ensureDirectoryExistence(fileName);

        const data = {startX: this.leapStartX, periodWidth: this.leapPeriodWidth};
        fs.writeFile(fileName, JSON.stringify(data), (err) => {
            if (err) {console.error(err);} // eslint-disable-line no-console
        });
    }
}

export default new LinearDetector();
