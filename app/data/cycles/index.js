import extractThresholds from "app/data/cycles/extractThresholds";
import arrangeDataToCycles from "app/data/cycles/arrangeDataToCycles";
import cycleTime from "app/data/cycles/cycleTime";

export default function index({thresholdList, dataByAxis}) {
    const thresholds = extractThresholds(thresholdList);
    const cycles = cycleTime({seconds: dataByAxis.seconds, thresholds});
    return arrangeDataToCycles({cycles, dataByAxis, thresholds});
}
