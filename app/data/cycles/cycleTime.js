import _ from "lodash";

export default function cycleTime({seconds, thresholds}) {
    return _.flatMap(thresholds, (startIndex, index) => {
        const endIndex = index + 1 < thresholds.length ? thresholds[index + 1] : undefined;

        if (startIndex === endIndex) {
            return [];
        }

        return [{
            time: diffTime(seconds, startIndex, endIndex),
            cycleNr: index,
        }];
    });
}

function diffTime(seconds, startIndex, endIndex) {
    // if endIndex is defined, it is exclusive and take previous
    // if endIndex is undefined, take last element
    const endTime = endIndex ? seconds[endIndex] : _.last(seconds);

    // take previous time to keep sum of cycles equal to total time.
    const startTime = seconds[startIndex];

    return endTime - startTime;
}
