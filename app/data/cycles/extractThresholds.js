import linearDetector from "app/data/cycles/LinearDetector";

/**
* Returns list of indexes. Negatives go to the 0 cycle.
* a[b] = c
* a - list of indexes
* b - number of cycle
* c - first index of x list element that goes to that cycle
*/
export default function extractThresholds(xList) {
    const thresholds = [0];
    let currentCycle = 0;

    xList.forEach((x, index) => {
        const cycleForX = linearDetector.getCycleNr(x);
        while (cycleForX > currentCycle) {
            currentCycle++;
            thresholds[currentCycle] = index;
        }
    });

    return thresholds;
}
