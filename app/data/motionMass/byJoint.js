import calculateMovementParams from "app/data/calculate/calculateMovementParams";
import summarize from "app/data/calculate/summarize";
import byCycle from "app/data/motionMass/byCycle";

export default function byJoint({dataByAxis, xPositions, joints}) {
    return joints.map(joint => {
        const result = calculateMovementParams({locations: dataByAxis[joint], times: dataByAxis.seconds});
        const {speeds, ...motionMassesByAxis} = result;
        return {
            joint,
            raw: {speeds, ...motionMassesByAxis},
            total: summarize(motionMassesByAxis),
            cycles: byCycle({dataByAxis, xPositions, joint}),
        };
    });
}
