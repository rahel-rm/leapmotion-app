import cycles from "app/data/cycles";
import calculateMovementParams from "app/data/calculate/calculateMovementParams";
import summarize from "app/data/calculate/summarize";

export default function byCycle({dataByAxis, xPositions, joint}) {
    const cycleData = cycles({thresholdList: xPositions, dataByAxis});
    return cycleData.map(cycle => {
        const motionMassesByCycleAndAxis = calculateMovementParams({locations: cycle[joint], times: cycle.seconds});
        return {
            cycleNr: cycle.cycleNr,
            total: {...summarize(motionMassesByCycleAndAxis), time: cycle.time},
            byAxis: {...motionMassesByCycleAndAxis, time: cycle.time},
        };
    });
}
