class PathBuffer {
    constructor() {
        this.path = [];
    }

    push({x, y}) {
        this.path.push({x, y});
    }

    get() {
        return this.path;
    }

    reset() {
        this.path = [];
    }
}

export default new PathBuffer();
