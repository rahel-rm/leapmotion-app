import _ from "lodash";
import extractFields from "app/data/extract/extractFields";
import transform from "app/data/transform";
import saveToFile from "app/data/storage/saveToFile";
import byJoint from "app/data/motionMass/byJoint";
import DTW from "app/data/motionMass/DTW";

export default class MotionMass {
    constructor(data, joints) {
        //  Let J = {j1, j2, ..., jn} be the set of the joints of interest.
        this.data = data;
        this.joints = joints;
    }

    calc() {
        if (!this.data.length) {return;}

        this.data = calculate(_.sortBy(this.data, "MicrosSinceStart"), this.joints);
    }

    getData() {
        return this.data;
    }
}

function calculate(data, joints) {
    const raw = extractFields({data, joints});
    const dataByAxis = transform({data, joints, rebaseFields: {base: "WristPosition", target: "IndexTipPosition"}});
    const dtwLocations = {x: raw.IndexTipPositionX, y: raw.IndexTipPositionY};

    const result = {
        raw,
        byJoint: byJoint({dataByAxis, xPositions: raw.IndexTipPositionX, joints}),
        dtw: DTW({locations: dtwLocations}),
    };

    saveToFile(result);

    return result;
}
