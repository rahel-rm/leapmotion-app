export default function convertTime(data) {
    return data.map(({MicrosSinceStart, ...rest}) => ({
        ...rest,
        seconds: (MicrosSinceStart / 1000 / 1000) || 0,
    }));
}
