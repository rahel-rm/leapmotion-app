import convertTime from "app/data/transform/convertTime";
import rebaseOneOnOther from "app/data/transform/rebaseOneOnOther";
import transpose from "app/data/transform/transpose";
import arrangeToAxes from "app/data/transform/arrangeToAxes";

export default function transform({data, joints, rebaseFields}) {
    const {base: baseKey, target: targetKey} = rebaseFields;
    const rebasedData = rebaseOneOnOther(data, targetKey, baseKey);
    const convertedTime = convertTime(rebasedData);
    const transposed = transpose(convertedTime);
    return arrangeToAxes(transposed, joints);
}
