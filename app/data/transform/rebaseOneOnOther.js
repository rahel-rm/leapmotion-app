import _ from "lodash";

export default function rebaseOneOnOther(data, target, base) {
    return data.map(frame => rebaseOneFrame(frame, target, base));
}

function rebaseOneFrame(frame, targetKey, baseKey) {
    const baseFields = pickAndKeepAxis(frame, baseKey);
    const targetFields = pickAndKeepAxis(frame, targetKey);

    const updatedFields = _.mapValues(targetFields, (targetValue, key) => rebase(targetValue, key, baseFields));
    return {...frame, ...putKeyBack(updatedFields, targetKey)};
}

function pickAndKeepAxis(frame, key) {
    const fields = _.pick(frame, suffixAxes(key));
    return _.mapKeys(fields, (v, k) => k.slice(-1));
}

function suffixAxes(field) {
    return [field + "X", field + "Y", field + "Z"];
}

function rebase(targetValue, key, baseFields) {
    return targetValue - baseFields[key];
}

function putKeyBack(fields, key) {
    return _.mapKeys(fields, (v, k) => key + k);
}
