import drawBox from "app/components/display/drawingBoard/drawBox";

const canvasW = drawBox.getCanvasWidth();

const pixelScale = 3;

const xDelta = canvasW / 2;
const yDelta = 930;

export default {
    toScreen: ({x, y}) => {
        const [left, top] = adjustFromLeap({x, y});
        return [left + drawBox.getLeftMargin(), top + drawBox.getTopMargin()];
    },

    toCanvas: ({x, y}) => {
        return adjustFromLeap({x, y});
    },

    canvasToLeapMotion: ({x, y}) => {
        return adjustToLeap({x, y});
    },

    screenToLeapMotion: ({x, y}) => {
        const [left, top] = [x - drawBox.getLeftMargin(), y - drawBox.getTopMargin()];
        return adjustToLeap({x: left, y: top});
    },

    getPixelScale: () => pixelScale,
};

function adjustToLeap({x, y}) {
    // x2 = x1 * a + b
    // x2 - b = x1 * a
    // x1 * a = x2 - b
    // x1 = (x2 - b) / a

    const newX = (x - xDelta) / pixelScale;
    const newY = (y - yDelta) / -pixelScale;

    return [roundToTenth(newX), roundToTenth(newY)];
}

function adjustFromLeap({x, y}) {
    // x = leapmotion y coordinate, y = screen y coordinate
    // if screen 0 = leapmotion 600
    // and screen height = leapmotion 0
    //
    // y = ax + b
    //
    // leap -> canvas
    // 210 -> -30 paras kõrgus leapile
    // 115 -> 250 joonistamiseks
    //
    // kasti kõrgus fiks!
    // vaja 210 -> 250 paras kõrgus ja joonistatava keskel

    // x2 = x1 * a + b
    // x2 - b = x1 * a
    // x1 * a = x2 - b
    // x1 = (x2 - b) / a

    const newX = x * pixelScale + xDelta;
    const newY = y * -pixelScale + yDelta;

    return [roundToTenth(newX), roundToTenth(newY)];
}

function roundToTenth(value) {
    return Math.round(10 * value) / 10;
}
