import _ from "lodash";

export default function arrangeToAxes(listObjects, fields) {
    return _.reduce(fields, arrange, listObjects);
}

function arrange(listObjects, field) {
    const {
        [field + "X"]: X,
        [field + "Y"]: Y,
        [field + "Z"]: Z,
        ...rest
    } = listObjects;

    return {
        [field]: {
            X, Y, Z,
        },
        ...rest,
    };
}
