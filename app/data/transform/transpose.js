import _ from "lodash";

export default function transpose(data) {
    const fields = _.keys(data[0]);
    const result = {};
    fields.forEach(key => {
        result[key] = extract(data, key);
    });
    return result;
}

function extract(data, key) {
    return _.map(data, (frame) => (frame[key]));
}
