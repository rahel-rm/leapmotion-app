// import drawFieldList from "app/data/fieldList/drawFieldList";
// import parseDrawFrame from "app/data/extract/parseDrawFrame"
import gestureFieldList from "app/data/fieldList/gestureFieldList";
import fullFieldList from "app/data/fieldList/fullFieldList";
import parseFrame from "app/data/extract/parseFrame";
import DataBuffer from "app/data/storage/DataBuffer";

export default function(test, bufferConfig) {
    switch (test) {
        case "grab":
        case "pinch":
            return {
                fieldList: gestureFieldList,
                parseFrame: parseFrame,
                dataBuffer: new DataBuffer(bufferConfig),
            };
        default:
            return {
                fieldList: fullFieldList,
                parseFrame: parseFrame,
                dataBuffer: new DataBuffer(bufferConfig),
            };
    }
}
