import _ from "lodash";
import {euclideanDistance} from "app/data/calculate/trigonometry";

export default function calculateMovementParams({locations, times}) {
    const distance = getEucDistFirstLast(locations);
    const timeDiffs = mapDiff(times);
    const distanceDiffs = mapValues(mapDiff, locations);
    const speeds = mapValues(mapDivide, distanceDiffs, timeDiffs);
    const accelerations = mapValues(divideDiffs, speeds, timeDiffs);
    const jerks = mapValues(divideDiffs, accelerations, timeDiffs);

    return {
        distance,
        distanceDiffs,
        speeds, accelerations, jerks,
    };
}

function mapValues(func, list, list2) {
    return _.mapValues(list, (el) => func(el, list2));
}

function getEucDistFirstLast({X, Y, Z}) {
    const firstFrame = [X, Y, Z].map(field => field[0]);
    const lastFrame = [X, Y, Z].map(field => _.last(field));
    return euclideanDistance(firstFrame, lastFrame);
}

function mapDiff(list) {
    return _.flatMap(list, (element, index) => {
        const prevEl = list[index - 1];
        return (element - prevEl) || 0;
    });
}

function mapDivide(full, divisor) {
    return _.map(full, (element, index) => {
        return (element / divisor[index]) || 0;
    });
}

function divideDiffs(diffee, divider) {
    return mapDivide(mapDiff(diffee), divider);
}
