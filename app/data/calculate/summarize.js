import _ from "lodash";
import {euclideanDistance} from "app/data/calculate/trigonometry";

export default function summarize({distanceDiffs, accelerations, jerks, ...rest}) {
    return {
        trajectory: getTotal(distanceDiffs),
        acceleration: getTotal(accelerations),
        jerk: getTotal(jerks),
        ...rest,
    };
}

function getTotal({X, Y, Z}) {
    const absX = _.map(X, (i) => Math.abs(i));
    const absY = _.map(Y, (i) => Math.abs(i));
    const absZ = _.map(Z, (i) => Math.abs(i));
    const sums = _.map([absX, absY, absZ], (el) => _.sum(el));
    return euclideanDistance([0, 0, 0], sums);
}
