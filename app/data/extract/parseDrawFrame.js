export default function(frame) {
    if (!frame || !frame.hands) {
        return;
    }

    if (frame.hands.length !== 1) {
        return;
        // throw new Error("Found 0 or more than 1 hand!");
    }

    const hand = frame.hands[0];
    if (!hand.valid) {
        throw new Error("Hand data not valid!");
    }

    return {
        ...armData(hand.arm),
        ...handData(hand),
        ...fingersData(hand.pointables),
    };
}

function handData(hand) {
    const {timeVisible, type} = hand;

    return {
        TimePalmVisible: timeVisible,
        HandType: type,
    };
}

function armData(arm) {
    const [WristPositionX, WristPositionY, WristPositionZ] = arm.nextJoint;

    return {
        WristPositionX, WristPositionY, WristPositionZ,
    };
}

function fingersData(pointables) {
    const types = ["Thumb", "Index", "Middle", "Ring", "Pinky"];

    return {
        ...fingerData(pointables[1], types[pointables[1].type]),
    };
}

function fingerData(finger, type) {
    if (!finger.valid) {
        return {};
    }

    const [tipPositionX, tipPositionY, tipPositionZ] = finger.tipPosition;
    const [stabilizedTipPositionX, stabilizedTipPositionY, stabilizedTipPositionZ] = finger.stabilizedTipPosition;
    const [tipDirectionX, tipDirectionY, tipDirectionZ] = finger.direction;
    const [tipVelocityX, tipVelocityY, tipVelocityZ] = finger.tipVelocity;
    const [mcpPositionX, mcpPositionY, mcpPositionZ] = finger.mcpPosition; // knuckle aka metacarpophalangeal joint
    const [pipPositionX, pipPositionY, pipPositionZ] = finger.pipPosition; // joint between middle and proximal
    const [dipPositionX, dipPositionY, dipPositionZ] = finger.dipPosition; // joint between distal and middle
    const [carpPositionX, carpPositionY, carpPositionZ] = finger.carpPosition; // joint between metacarpal and wrist
    const {length, width, extended} = finger;

    // findRest(finger,
    //     ["tipPosition", "stabilizedTipPosition", "direction", "tipVelocity", "length", "width",
    //         "mcpPosition", "pipPosition", "dipPosition", "carpPosition", "type", "extended",
    //         "metacarpal", "proximal", "medial", "distal"],
    //     ["valid", "id", "handId", "tool", "touchZone", "touchDistance", "timeVisible",
    //         "finger", "positions", "bones"]);

    // ["finger", "_center", "_matrix", "_left"]

    return {
        [type + "TipVelocityX"]: tipVelocityX,
        [type + "TipVelocityY"]: tipVelocityY,
        [type + "TipVelocityZ"]: tipVelocityZ,
        [type + "TipPositionX"]: tipPositionX,
        [type + "TipPositionY"]: tipPositionY,
        [type + "TipPositionZ"]: tipPositionZ,
        [type + "StabilizedTipPositionX"]: stabilizedTipPositionX,
        [type + "StabilizedTipPositionY"]: stabilizedTipPositionY,
        [type + "StabilizedTipPositionZ"]: stabilizedTipPositionZ,
        [type + "TipDirectionX"]: tipDirectionX,
        [type + "TipDirectionY"]: tipDirectionY,
        [type + "TipDirectionZ"]: tipDirectionZ,
        [type + "McpPositionX"]: mcpPositionX,
        [type + "McpPositionY"]: mcpPositionY,
        [type + "McpPositionZ"]: mcpPositionZ,
        [type + "PipPositionX"]: pipPositionX,
        [type + "PipPositionY"]: pipPositionY,
        [type + "PipPositionZ"]: pipPositionZ,
        [type + "DipPositionX"]: dipPositionX,
        [type + "DipPositionY"]: dipPositionY,
        [type + "DipPositionZ"]: dipPositionZ,
        [type + "CarpPositionX"]: carpPositionX,
        [type + "CarpPositionY"]: carpPositionY,
        [type + "CarpPositionZ"]: carpPositionZ,
        [type + "Length"]: length,
        [type + "Width"]: width,
        [type + "Extended"]: extended,
        ...fingerBone(finger.metacarpal, type + "Metacarpal"),
        ...fingerBone(finger.proximal, type + "Proximal"),
        ...fingerBone(finger.medial, type + "Medial"),
        ...fingerBone(finger.distal, type + "Distal"),
    };
}

function fingerBone(bone, type) {
    const {width, length, basis} = bone;
    const [[mcXX, mcXY, mcXZ], [mcYX, mcYY, mcYZ], [mcZX, mcZY, mcZZ]] = basis;

    return {
        [type + "Width"]: width,
        [type + "Length"]: length,
        [type + "BasisXX"]: mcXX,
        [type + "BasisXY"]: mcXY,
        [type + "BasisXZ"]: mcXZ,
        [type + "BasisYX"]: mcYX,
        [type + "BasisYY"]: mcYY,
        [type + "BasisYZ"]: mcYZ,
        [type + "BasisZX"]: mcZX,
        [type + "BasisZY"]: mcZY,
        [type + "BasisZZ"]: mcZZ,
    };
}
