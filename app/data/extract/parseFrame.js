// import _ from "lodash";

export default function(frame) {
    if (!frame || !frame.hands) {
        return;
    }

    if (frame.hands.length !== 1) {
        return;
        // throw new Error("Found 0 or more than 1 hand!");
    }

    const hand = frame.hands[0];
    if (!hand.valid) {
        throw new Error("Hand data not valid!");
    }

    return {
        ...armData(hand.arm),
        ...handData(hand),
        ...fingersData(hand.pointables),
    };
}

function handData(hand) {
    const [StabilizedPalmPositionX, StabilizedPalmPositionY, StabilizedPalmPositionZ] = hand.stabilizedPalmPosition;
    const [PalmPositionX, PalmPositionY, PalmPositionZ] = hand.palmPosition;
    const [PalmDirectionX, PalmDirectionY, PalmDirectionZ] = hand.direction;
    const [PalmVelocityX, PalmVelocityY, PalmVelocityZ] = hand.palmVelocity;
    const [PalmNormalX, PalmNormalY, PalmNormalZ] = hand.palmNormal;
    const [SphereCenterX, SphereCenterY, SphereCenterZ] = hand.sphereCenter || [0, 0, 0];
    const {sphereRadius, confidence, grabStrength, pinchStrength, timeVisible, type} = hand;
    const PalmPitch = hand.pitch();
    const PalmRoll = hand.roll();
    const PalmYaw = hand.yaw();

    // findRest(hand,
    //     ["stabilizedPalmPosition", "palmPosition", "direction", "palmVelocity", "palmNormal", "sphereCenter",
    //         "sphereRadius", "confidence", "grabStrength", "pinchStrength", "timeVisible", "type"],
    //     ["arm", "pointables", "fingers", "tools", "id", "valid", "roll", "yaw", "pitch", "thumb",
    //         "indexFinger", "middleFinger", "ringFinger", "pinky"]);

    // ["_translation", "_rotation", "_scaleFactor", "frame", "finger", "rotationAngle", "rotationAxis",
    //  "rotationMatrix", "scaleFactor", "translation", "toString", "data", "hold", "holding", "release",
    //  "hoverFn", "hovering"]

    return {
        PalmVelocityX, PalmVelocityY, PalmVelocityZ,
        StabilizedPalmPositionX, StabilizedPalmPositionY, StabilizedPalmPositionZ,
        PalmPositionX, PalmPositionY, PalmPositionZ,
        PalmDirectionX, PalmDirectionY, PalmDirectionZ,
        PalmNormalX, PalmNormalY, PalmNormalZ,
        PalmRoll, PalmPitch, PalmYaw,
        SphereCenterX, SphereCenterY, SphereCenterZ,
        SphereRadius: sphereRadius,
        PalmConfidence: confidence,
        GrabStrength: grabStrength,
        PinchStrength: pinchStrength,
        TimePalmVisible: timeVisible,
        HandType: type,
    };
}

function armData(arm) {
    const {width, length} = arm;
    const [[basisXX, basisXY, basisXZ], [basisYX, basisYY, basisYZ], [basisZX, basisZY, basisZZ]] = arm.basis;
    const [WristPositionX, WristPositionY, WristPositionZ] = arm.nextJoint;
    const [ElbowPositionX, ElbowPositionY, ElbowPositionZ] = arm.prevJoint;

    // const [ArmDirectionX, ArmDirectionY, ArmDirectionZ] = arm.direction(); //  === -basis[2]
    /** Lerp:  Calculates the coordinates of the point between the ends of the bone in millimeters
                from the Leap Motion origin by linearly interpolating between the end points of the
                bone. If the t parameter is set to 0, the position corresponds to the prevJoint; if
                set to 1, the position corresponds to nextJoint.
    */

    // findRest(arm,
    //     ["width", "length", "basis", "nextJoint", "prevJoint"],
    //     ["center", "direction", "left", "lerp", "type", "matrix"]);

    // ["finger", "_center", "_matrix", "_left"]

    return {
        ArmWidth: width, ArmLength: length,
        WristBasisXX: basisXX, WristBasisXY: basisXY, WristBasisXZ: basisXZ,
        WristBasisYX: basisYX, WristBasisYY: basisYY, WristBasisYZ: basisYZ,
        WristBasisZX: basisZX, WristBasisZY: basisZY, WristBasisZZ: basisZZ,
        WristPositionX, WristPositionY, WristPositionZ,
        ElbowPositionX, ElbowPositionY, ElbowPositionZ,
        // ArmDirectionX, ArmDirectionY, ArmDirectionZ,
    };
}

function fingersData(pointables) {
    const types = ["Thumb", "Index", "Middle", "Ring", "Pinky"];

    return {
        ...fingerData(pointables[0], types[pointables[0].type]),
        ...fingerData(pointables[1], types[pointables[1].type]),
        ...fingerData(pointables[2], types[pointables[2].type]),
        ...fingerData(pointables[3], types[pointables[3].type]),
        ...fingerData(pointables[4], types[pointables[4].type]),
    };
}

function fingerData(finger, type) {
    if (!finger.valid) {
        return {};
    }

    const [tipPositionX, tipPositionY, tipPositionZ] = finger.tipPosition;
    const [stabilizedTipPositionX, stabilizedTipPositionY, stabilizedTipPositionZ] = finger.stabilizedTipPosition;
    const [tipDirectionX, tipDirectionY, tipDirectionZ] = finger.direction;
    const [tipVelocityX, tipVelocityY, tipVelocityZ] = finger.tipVelocity;
    const [mcpPositionX, mcpPositionY, mcpPositionZ] = finger.mcpPosition; // knuckle aka metacarpophalangeal joint
    const [pipPositionX, pipPositionY, pipPositionZ] = finger.pipPosition; // joint between middle and proximal
    const [dipPositionX, dipPositionY, dipPositionZ] = finger.dipPosition; // joint between distal and middle
    const [carpPositionX, carpPositionY, carpPositionZ] = finger.carpPosition; // joint between metacarpal and wrist
    const {length, width, extended} = finger;

    // findRest(finger,
    //     ["tipPosition", "stabilizedTipPosition", "direction", "tipVelocity", "length", "width",
    //         "mcpPosition", "pipPosition", "dipPosition", "carpPosition", "type", "extended",
    //         "metacarpal", "proximal", "medial", "distal"],
    //     ["valid", "id", "handId", "tool", "touchZone", "touchDistance", "timeVisible",
    //         "finger", "positions", "bones"]);

    // ["finger", "_center", "_matrix", "_left"]

    return {
        [type + "TipVelocityX"]: tipVelocityX,
        [type + "TipVelocityY"]: tipVelocityY,
        [type + "TipVelocityZ"]: tipVelocityZ,
        [type + "TipPositionX"]: tipPositionX,
        [type + "TipPositionY"]: tipPositionY,
        [type + "TipPositionZ"]: tipPositionZ,
        [type + "StabilizedTipPositionX"]: stabilizedTipPositionX,
        [type + "StabilizedTipPositionY"]: stabilizedTipPositionY,
        [type + "StabilizedTipPositionZ"]: stabilizedTipPositionZ,
        [type + "TipDirectionX"]: tipDirectionX,
        [type + "TipDirectionY"]: tipDirectionY,
        [type + "TipDirectionZ"]: tipDirectionZ,
        [type + "McpPositionX"]: mcpPositionX,
        [type + "McpPositionY"]: mcpPositionY,
        [type + "McpPositionZ"]: mcpPositionZ,
        [type + "PipPositionX"]: pipPositionX,
        [type + "PipPositionY"]: pipPositionY,
        [type + "PipPositionZ"]: pipPositionZ,
        [type + "DipPositionX"]: dipPositionX,
        [type + "DipPositionY"]: dipPositionY,
        [type + "DipPositionZ"]: dipPositionZ,
        [type + "CarpPositionX"]: carpPositionX,
        [type + "CarpPositionY"]: carpPositionY,
        [type + "CarpPositionZ"]: carpPositionZ,
        [type + "Length"]: length,
        [type + "Width"]: width,
        [type + "Extended"]: extended,
        ...fingerBone(finger.metacarpal, type + "Metacarpal"),
        ...fingerBone(finger.proximal, type + "Proximal"),
        ...fingerBone(finger.medial, type + "Medial"),
        ...fingerBone(finger.distal, type + "Distal"),
    };
}

function fingerBone(bone, type) {
    const {width, length, basis} = bone;
    const [[mcXX, mcXY, mcXZ], [mcYX, mcYY, mcYZ], [mcZX, mcZY, mcZZ]] = basis;

    return {
        [type + "Width"]: width,
        [type + "Length"]: length,
        [type + "BasisXX"]: mcXX,
        [type + "BasisXY"]: mcXY,
        [type + "BasisXZ"]: mcXZ,
        [type + "BasisYX"]: mcYX,
        [type + "BasisYY"]: mcYY,
        [type + "BasisYZ"]: mcYZ,
        [type + "BasisZX"]: mcZX,
        [type + "BasisZY"]: mcZY,
        [type + "BasisZZ"]: mcZZ,
    };
}

// function findRest(params, chosen, excluded) {
//     // eslint-disable-line no-unused-vars
//     const omittedParams = _.omit(params, [...chosen, ...excluded]);
//     const keys = Object.keys(omittedParams);
//     console.log("restParams", omittedParams, keys); // eslint-disable-line no-console
// }

// WristPositionX	WristPositionY	WristPositionZ
// GrabStrength	GrabAngle	Confidence
// ThumbProximalJointX	ThumbProximalJointY	ThumbProximalJointZ
// ThumbIntermediateJointX	ThumbIntermediateJointY ThumbIntermediateJointZ
// ThumbDistalJointX	ThumbDistalJointY	ThumbDistalJointZ
// ThumbTipPositionX	ThumbTipPositionY	ThumbTipPositionZ
// ThumbDirectionPitch	ThumbDirectionYaw	ThumbDirectionRoll
// ThumbTipVelocityX	ThumbTipVelocityY	ThumbTipVelocityZ

// IndexMetacarpalJointX	IndexMetacarpalJointY	IndexMetacarpalJointZ
// IndexProximalJointX	IndexProximalJointY	IndexProximalJointZ
// IndexIntermediateJointX	IndexIntermediateJointY	IndexIntermediateJointZ
// IndexDistalJointX	IndexDistalJointY	IndexDistalJointZ
// IndexTipPositionX	IndexTipPositionY	IndexTipPositionZ
// IndexDirectionPitch	IndexDirectionYaw	IndexDirectionRoll
// IndexTipVelocityX	IndexTipVelocityY	IndexTipVelocityZ

// MiddleMetacarpalJointX	MiddleMetacarpalJointY	MiddleMetacarpalJointZ
// MiddleProximalJointX	MiddleProximalJointY	MiddleProximalJointZ
// MiddleIntermediateJointX	MiddleIntermediateJointY	MiddleIntermediateJointZ
// MiddleDistalJointX	MiddleDistalJointY	MiddleDistalJointZ
// MiddleTipPositionX	MiddleTipPositionY	MiddleTipPositionZ
// MiddleDirectionPitch	MiddleDirectionYaw	MiddleDirectionRoll
// MiddleTipVelocityX	MiddleTipVelocityY	MiddleTipVelocityZ

// RingMetacarpalJointX	RingMetacarpalJointY	RingMetacarpalJointZ
// RingProximalJointX	RingProximalJointY	RingProximalJointZ
// RingIntermediateJointX	RingIntermediateJointY	RingIntermediateJointZ
// RingDistalJointX	RingDistalJointY	RingDistalJointZ
// RingTipPositionX	RingTipPositionY	RingTipPositionZ
// RingDirectionPitch	RingDirectionYaw	RingDirectionRoll
// RingTipVelocityX	RingTipVelocityY	RingTipVelocityZ

// PinkyMetacarpalJointX	PinkyMetacarpalJointY	PinkyMetacarpalJointZ
// PinkyProximalJointX	PinkyProximalJointY	PinkyProximalJointZ
// PinkyIntermediateJointX	PinkyIntermediateJointY	PinkyIntermediateJointZ
// PinkyDistalJointX	PinkyDistalJointY	PinkyDistalJointZ
// PinkyTipPositionX	PinkyTipPositionY	PinkyTipPositionZ
// PinkyDirectionPitch	PinkyDirectionYaw	PinkyDirectionRoll
// PinkyTipVelocityX	PinkyTipVelocityY	PinkyTipVelocityZ

// PalmDirectionPitch	PalmDirectionYaw	PalmDirectionRoll
// ArmDirectionPitch	ArmDirectionYaw	ArmDirectionRoll	MicrosSinceStart
