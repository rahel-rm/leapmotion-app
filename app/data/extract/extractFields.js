import _ from "lodash";
import {euclideanDistance} from "app/data/calculate/trigonometry";

export default function extractFields({data, joints}) {
    const grabStrength = getField(data, "GrabStrength", "");
    const pinchStrength = getField(data, "PinchStrength", "");

    const leapVelocityX = getField(data, "IndexTipVelocity", "X");
    const leapVelocityY = getField(data, "IndexTipVelocity", "Y");
    const leapVelocityZ = getField(data, "IndexTipVelocity", "Z");
    const leapVelocityEucl = getLeapVelocityEucl({leapVelocityX, leapVelocityY, leapVelocityZ});

    const simples = joints.reduce((current, joint) => {
        return {...current, ...simpleCoords(data, joint)};
    }, {});

    return {
        grabStrength, pinchStrength,
        ...simples,
        leapVelocityX, leapVelocityY, leapVelocityZ, leapVelocityEucl,
    };
}

function simpleCoords(data, joint) {
    return {
        [joint + "X"]: getField(data, joint, "X"),
        [joint + "Y"]: getField(data, joint, "Y"),
        [joint + "Z"]: getField(data, joint, "Z"),
        [joint + "XDelta"]: getFieldDiff(data, joint, "X"),
        [joint + "YDelta"]: getFieldDiff(data, joint, "Y"),
        [joint + "ZDelta"]: getFieldDiff(data, joint, "Z"),
    };
}

function getField(data, joint, coord) {
    return _.map(data, (frame) => frame[joint + coord]);
}

function getFieldDiff(data, joint, coord) {
    return _.map(data, (frame, index, data) => {
        const prevFrame = _.get(data, [index - 1], {});
        return (frame[joint + coord] - prevFrame[joint + coord]) || 0;
    });
}

function getLeapVelocityEucl({leapVelocityX, leapVelocityY, leapVelocityZ}) {
    return _.map(leapVelocityX, (frame, index) => {
        if (index === 0) {
            return 0;
        }

        const f1 = [leapVelocityX[index - 1], leapVelocityY[index - 1], leapVelocityZ[index - 1]];
        const f2 = [leapVelocityX[index], leapVelocityY[index], leapVelocityZ[index]];
        return euclideanDistance(f1, f2);
    });
}
