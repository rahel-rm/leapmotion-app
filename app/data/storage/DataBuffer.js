// import getData from "app/data/motionMass/getData";

export default class DataBuffer {
    constructor() {
        this.buffer = [];
    }

    save(rawData) {
        const {
            FrameNr,
            MicrosSinceStart,
            IndexTipPositionX,
            IndexTipPositionY,
            IndexTipPositionZ,
            IndexTipVelocityX,
            IndexTipVelocityY,
            IndexTipVelocityZ,
            WristPositionX,
            WristPositionY,
            WristPositionZ,
            GrabStrength,
            PinchStrength,
        } = rawData;
        this.buffer.push({
            FrameNr,
            MicrosSinceStart,
            IndexTipPositionX,
            IndexTipPositionY,
            IndexTipPositionZ,
            IndexTipVelocityX,
            IndexTipVelocityY,
            IndexTipVelocityZ,
            WristPositionX,
            WristPositionY,
            WristPositionZ,
            GrabStrength,
            PinchStrength,
        });
    }

    get() {
        return this.buffer;
        // return getData();
    }
}
