import _ from "lodash";
import path from "path";
import fs from "fs";
import moment from "moment";
import stringify from "csv-stringify";
import {store} from "app/store/store";
import getTestSpecifics from "app/data/getTestSpecifics";
import MetadataManager from "app/data/storage/MetadataManager";
import winCmd from "app/leapmotion/winCmd";
import LeapMotion from "app/leapmotion/Controller";

export default class LeapMotionFileWriter {
    constructor(controller) {
        this.controller = controller;
        this.frameNr = 0;

        this.test = store.getState().stage.test;
        const {fieldList, parseFrame, dataBuffer} = getTestSpecifics(this.test);
        this.fieldList = fieldList;
        this.parseFrame = parseFrame;
        this.dataBuffer = dataBuffer;

        const metadata = {
            subjectCode: store.getState().subject.code,
        };

        this.logFrames(metadata);
    }

    logFrames(metadata) {
        this.frameListener = (frame) => {
            if (!LeapMotion.isFrameStreamed(frame) || !this.shouldSaveData()) {
                return;
            }

            const parsedFrame = this.parseFrame(frame);
            if (parsedFrame) {
                let first = false;
                if (!this.writeStream) {
                    const options = {
                        flags: "a",
                        highWaterMark: 50 * 1024 * 1024,
                    };
                    this.writeStream = fs.createWriteStream(this.getFileName(), options);
                    this.writeLineToFile(this.fieldList);
                    this.metadata = new MetadataManager(metadata, frame);
                    first = true;
                }
                const fullFrame = {...this.metadata.format(frame, first), ...parsedFrame};
                this.writeLineToFile(this.prepareLine(fullFrame));
                this.dataBuffer.save(fullFrame);
            }
        };

        this.controller.on("frame", this.frameListener);
    }

    shouldSaveData() {
        return store.getState().stage.saving;
    }

    getFileName() {
        const ms = moment().format("YYYY-MM-DD-THHmmss");
        const home = winCmd.getUserHome();
        const fileName = path.join(home, "thesis", "data", `leapmotion-data-${this.test}-${ms}.csv`);
        winCmd.ensureDirectoryExistence(fileName);
        return fileName;
    }

    writeLineToFile(content) {
        stringify([content], {rowDelimiter: "windows"}, (err, output) => {
            if (this.writeStream && !this.writeStream.write(output)) {
                // this.writeStream.once("drain", () => this.writeLineToFile(content));
                console.warn("Draining"); // eslint-disable-line no-console
            }
        });
    }

    stop() {
        this.controller.removeListener("frame", this.frameListener);
        this.writeStream && this.writeStream.end();
        this.writeStream = undefined;
    }

    getBuffer() {
        return this.dataBuffer;
    }

    prepareLine(data) {
        const dataList = this.fieldList.map((field) => {
            if (_.isNumber(data[field]) && !_.isInteger(data[field])) {
                return data[field].toPrecision(4);
            }
            return data[field];
        });

        return dataList;
    }
}
