import _ from "lodash";
import moment from "moment";

export default class MetadataManager {
    constructor(meta, firstFrame) {
        _.assign(this, meta, {
            firstFrameTime: firstFrame.timestamp,
            firstFrameHistoryIdx: firstFrame.historyIdx,
        });
    }

    format(frame) {
        return {
            Time: moment().format("YYYY-MM-DD H:mm:ss.SSS"),
            MicrosSinceStart: frame.timestamp - this.firstFrameTime,
            FrameNr: frame.historyIdx - this.firstFrameHistoryIdx,
            SubjectCode: this.subjectCode,
        };
    }
}
