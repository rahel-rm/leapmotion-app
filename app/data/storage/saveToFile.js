import _ from "lodash";
import path from "path";
import moment from "moment";
import fs from "fs";
import stringify from "csv-stringify";
import winCmd from "app/leapmotion/winCmd";

export default function(data, joint) {
    writeToFile(formatData(data), joint);
}

function formatData({
    data, timeDiffs,
    distanceDiffs, speeds, accelerations, jerks,
    x, y, z, deltaX, deltaY, deltaZ,
    leapVelocityX, leapVelocityY, leapVelocityZ, leapVelocityEucl,
}) {
    const microsSinceStart = _.map(data, (frame) => frame.MicrosSinceStart);

    const fields = [
        "microsSinceStart", "timeDiffs",
        "x", "y", "z",
        "deltaX", "deltaY", "deltaZ",
        "distanceDiffs", "speeds", "accelerations", "jerks",
        "leapVelocityX", "leapVelocityY", "leapVelocityZ", "leapVelocityEucl",
    ];
    const zip = _.zip(
        microsSinceStart, timeDiffs,
        x, y, z,
        deltaX, deltaY, deltaZ,
        distanceDiffs, speeds, accelerations, jerks,
        leapVelocityX, leapVelocityY, leapVelocityZ, leapVelocityEucl
    );

    const rounded = _.map(zip, (field) => {
        return _.map(field, (element) => {
            if (_.isNumber(element) && !_.isInteger(data[field])) {
                return element.toPrecision(4);
            }
            return element;
        });
    });

    return [fields, ...rounded];
}

function writeToFile(data, joint) {
    stringify(data, {rowDelimiter: "windows"}, (err, output) => {
        const fileName = getFileName(joint);
        fs.writeFile(fileName, output, (err) => {
            if (err) {console.error(err);} // eslint-disable-line no-console
        });
    });
}

function getFileName(joint) {
    const time = moment().format("YYYY-MM-DD-THHmmss");
    const home = winCmd.getUserHome();
    const fileName = path.join(home, "thesis", "calculations", `calculations-${joint}-${time}.csv`);
    winCmd.ensureDirectoryExistence(fileName);
    return fileName;
}
