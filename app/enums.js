export default {
    pages: {
        HOME: "home",
        MENU: "menu",
        TEST: "test",
        RESULTS: "results",
    },

    subTypes: {
        TIMES: "times",
        SPEED: "speed",
        BLIND: "blind",
        CONTINUE: "continue",
        VISIBLE: "visible",
    },
};
