export const ON_GO_TO_PAGE = "ON_GO_TO_PAGE";
export const ON_START_TEST = "ON_START_TEST";
export const ON_START_TRACKING = "ON_START_TRACKING";
export const ON_INCREMENT_COUNT = "ON_INCREMENT_COUNT";
export const ON_START_TEST_TIMER = "ON_START_TEST_TIMER";
export const ON_SET_RESULT_DATA = "ON_SET_RESULT_DATA";

export function onGoToPage(page) {
    return {
        type: ON_GO_TO_PAGE,
        nextPage: page,
    };
}

export function onStartTest(testName, subType) {
    return {
        type: ON_START_TEST,
        test: testName,
        subType: subType,
    };
}

export function onStartTracking() {
    return {
        type: ON_START_TRACKING,
    };
}

export function onIncrementCount() {
    return {
        type: ON_INCREMENT_COUNT,
    };
}

export function onStartTestTimer() {
    return {
        type: ON_START_TEST_TIMER,
    };
}

export function onSetResultData(data) {
    return {
        type: ON_SET_RESULT_DATA,
        data,
    };
}
