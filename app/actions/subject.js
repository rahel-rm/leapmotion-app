export const ON_SAVE_SUBJECT_CODE = "ON_SAVE_SUBJECT_CODE";
export const ON_CHANGE_SUBJECT_CODE = "ON_CHANGE_SUBJECT_CODE";

export function onSaveSubjectCode() {
    return {
        type: ON_SAVE_SUBJECT_CODE,
    };
}

export function onChangeSubjectCode(code) {
    return {
        type: ON_CHANGE_SUBJECT_CODE,
        code,
    };
}
