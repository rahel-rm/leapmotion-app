import {configureStore} from "app/store/configureStore";

const store = configureStore();

export default {store};
