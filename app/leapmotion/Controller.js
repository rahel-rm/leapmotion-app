import LeapJS from "leapjs";
import checkSystemDependencies from "app/leapmotion/checkSystemDependencies";
import winCmd from "app/leapmotion/winCmd";

class Controller {
    constructor() {
        if (winCmd.isWindows()) {
            checkSystemDependencies();
            this.runService();
        }
        this.controller = this.initController();
    }

    initController() {
        const controllerOptions = {
            frameEventName: "deviceFrame",
            inNode: false,
        };

        return new LeapJS.Controller(controllerOptions);
    }

    connect() {
        this.controller && this.controller.connect();
    }

    disconnect() {
        this.controller && this.controller.disconnect();
    }

    streaming() {
        const frame = this.controller && this.controller.frame();
        return this.isFrameStreamed(frame);
    }

    isFrameStreamed(frame) {
        return frame.data && !frame.data.playback;
    }

    getController() {
        return this.controller;
    }

    runService() {
        winCmd.runBatchFile("runServiceOnce", {async: true}).on("exit", () => {
            console.info("Service exited, running again."); // eslint-disable-line no-console
            setTimeout(() => {
                this.runService();
            }, 1000);
        });
    }
}

export default new Controller();
