/* eslint-disable no-console */
import winCmd from "app/leapmotion/winCmd";

export default function checkSystemDependencies() {
    if (!areRequirementsInstalled()) {
        console.info("Installation was rejected or not done. Trying to install.");

        if (install()) {
            console.info("Installation succeeded?");
        }
        else {
            console.info("Installation failed or rejected.");
        }
    }
}

function install() {
    const output = winCmd.runBatchFile("elevateInstall").stdout;
    return output.indexOf("Elevation successful") > -1;
}

function areRequirementsInstalled() {
    return winCmd.runBatchFile("checkRequirements").stdout.indexOf("Installing") === -1;
}
