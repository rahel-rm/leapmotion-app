/* eslint-disable no-console */
import {spawn, spawnSync} from "child_process";
import path from "path";
import fs from "fs";

export default {
    getCoreServiceCwd,
    detectArch,
    runBatchFile,
    ensureDirectoryExistence,
    runInCmd,
    getCwd,
    isWindows,
    isMac,
    getUserHome,
};

function getCwd() {
    return process.env.PORTABLE_EXECUTABLE_DIR ||
        path.join(__dirname, "..", "resources");
}

function ensureDirectoryExistence(filePath) {
    const dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
}

function getUserHome() {
    return process.env.HOME;
}

function isMac() {
    return process.platform === "darwin";
}

function getCoreServiceCwd() {
    return path.join(getCwd(), "leapmotion-service", "Core Services");
}

function runInCmd(command, {async} = {}) {
    if (!isWindows()) {
        return;
    }

    const cwd = getCoreServiceCwd();
    if (async) {
        return spawn("cmd.exe", ["/c", command], {cwd});
    }
    else {
        const bat = spawnSync("cmd.exe", ["/c", command], {cwd});
        const {status, stdout, stderr, error} = bat;
        const result = {status, stdout: stdout && stdout.toString(), stderr: stderr && stderr.toString(), error};
        console.warn("Running command:", command, "\n\nCWD:", cwd, "\n\nResult:", result);
        return result;
    }
}

function runBatchFile(fileName, options) {
    const file = path.join(getCoreServiceCwd(), fileName + ".bat");
    return runInCmd(file, options);
}

function detectArch() {
    const command = "reg Query \"HKLM\\Hardware\\Description\\System\\CentralProcessor\\0\" | find /i \"x86\"";
    const result = runInCmd(command).stdout;
    return result.length ? "x86" : "x64";
}

function isWindows() {
    return process.platform === "win32";
}
