import React from "react";
import {configure, mount} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {Provider} from "react-redux";
import HomePage from "app/components/pages/HomePage";
import {configureStore} from "app/store/configureStore";

function setup(initialState) {
    configure({adapter: new Adapter()});
    const store = configureStore(initialState);
    const app = mount(
        <Provider store={store}>
            <HomePage />
        </Provider>
    );
    return {
        app,
        input: app.find("input").at(0),
        button: app.find("button").at(0),
    };
}

describe("containers", () => {
    describe("App", () => {
        it("should load page with empty input ", () => {
            const {input} = setup();
            expect(input.getElement()).toMatchSnapshot();
        });

        it("should load page with button", () => {
            const {button} = setup();
            expect(button.getElement()).toMatchSnapshot();
        });

        it("should display updated input after typing", () => {
            const {input} = setup();
            input.simulate("change", {target: {value: "a"}});
            expect(input.instance().value).toMatch(/^a$/);
        });
    });
});
