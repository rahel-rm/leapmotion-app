import drawBox from "app/components/display/drawingBoard/drawBox";

describe("drawBox", () => {
    function extract() {
        return {
            getCanvasStyle: drawBox.getCanvasStyle(),

            getTopMargin: drawBox.getTopMargin(),
            getLeftMargin: drawBox.getLeftMargin(),

            getPageHeight: drawBox.getPageHeight(),
            getPageWidth: drawBox.getPageWidth(),

            getCanvasHeight: drawBox.getCanvasHeight(),
            getCanvasWidth: drawBox.getCanvasWidth(),

            getLeftToCenterThingToPage: drawBox.getLeftToCenterThingToPage(200),
            getTopToCenterThingToPage: drawBox.getTopToCenterThingToPage(300),
        };
    }

    describe("with FullHD screen", () => {
        beforeEach(() => {
            drawBox.__set__({
                doc: {
                    getWidth: () => 1920,
                    getHeight: () => 1080,
                },
            });
        });

        it("returns correct stuff", () => {
            expect(extract()).toMatchSnapshot();
        });
    });

    describe("with 1920x1200 screen", () => {
        beforeEach(() => {
            drawBox.__set__({
                doc: {
                    getWidth: () => 1920,
                    getHeight: () => 1200,
                },
            });
        });

        it("returns correct stuff", () => {
            expect(extract()).toMatchSnapshot();
        });
    });

    describe("with small screen", () => {
        beforeEach(() => {
            drawBox.__set__({
                doc: {
                    getWidth: () => 1280,
                    getHeight: () => 1024,
                },
            });
        });

        it("returns correct stuff", () => {
            expect(extract()).toMatchSnapshot();
        });
    });
});
