import React from "react";
import renderer from "react-test-renderer";
import CycleData from "app/components/display/result/CycleData";

function renderPage(props) {
    return renderer.create(<CycleData {...props} />);
}

describe("<CycleData />", () => {
    beforeEach(function() {
        CycleData.__set__({
            PlExample: () => (<div />),
        });
    });

    it("should render example and table", () => {
        const page = renderPage({
            data: [{
                cycleNr: 1,
                time: 5000000,
                trajectory: 10,
                acceleration: 15,
                accPerTime: 3,

            }],
            test: "PL",
        }).toJSON();
        expect(page).toMatchSnapshot();
    });

    it("renders nothing when test type is wrong", function() {
        const page = renderPage({
            data: [],
            test: "wrong",
        }).toJSON();
        expect(page).toMatchSnapshot();
    });
});
