import _ from "lodash";
import drawBox from "app/components/display/drawingBoard/drawBox";
import adjustCoordinates from "app/data/transform/adjustCoordinates";

// Y leapmotion 100 to 600 is bottom to top
describe("adjustCoordinates", () => {
    function adjustsTo({x, y, screenW, screenH}) {
        describe(`with screen size ${screenW}x${screenH}`, () => {
            beforeEach(() => {
                drawBox.__set__({
                    doc: {
                        getWidth: () => screenW,
                        getHeight: () => screenH,
                    },
                });
            });
            it(`adjusts from LeapMotion (${x}; ${y}) to canvas`, () => {
                expect(adjustCoordinates.toCanvas({x, y})).toMatchSnapshot();
            });

            it(`adjusts from LeapMotion (${x}; ${y}) to screen`, () => {
                expect(adjustCoordinates.toScreen({x, y})).toMatchSnapshot();
            });

            it(`adjusts from canvas (${x}; ${y}) to LeapMotion`, () => {
                expect(adjustCoordinates.canvasToLeapMotion({x, y})).toMatchSnapshot();
            });

            it(`adjusts from LeapMotion (${x}; ${y}) to canvas and back`, () => {
                const [canvasX, canvasY] = adjustCoordinates.toCanvas({x, y});
                expect(adjustCoordinates.canvasToLeapMotion({x: canvasX, y: canvasY})).toEqual([x, y]);
            });

            it(`adjusts from LeapMotion (${x}; ${y}) to screen and back`, () => {
                const [screenX, screenY] = adjustCoordinates.toScreen({x, y});
                expect(adjustCoordinates.screenToLeapMotion({x: screenX, y: screenY})).toEqual([x, y]);
            });
        });
    }

    function getMatrix() {
        const screens = [];
        const addScreenSize = (screenW, screenH) => screens.push({screenW, screenH});

        const locations = [];
        const addLocation = (x, y) => locations.push({x, y});

        addScreenSize(1280, 1024);
        addScreenSize(1920, 1200);

        addLocation(0, 200);
        addLocation(-150, 300);
        addLocation(150, 100);
        addLocation(250, 30);

        return _.flatMap(locations, (location) => {
            return screens.map((screen) => {
                return {...screen, ...location};
            });
        });
    }

    getMatrix().forEach(coords => adjustsTo(coords));
});
