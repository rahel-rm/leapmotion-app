import trigonometry from "app/data/calculate/trigonometry";

describe("trigonometry", function() {
    it("returns correct", function() {
        expect(trigonometry.euclideanDistance([17, 10, 0], [21, 13, 0])).toBe(5);
        expect(trigonometry.euclideanDistance([17, 10, 6], [17, 13, 10])).toBe(5);
    });
});
