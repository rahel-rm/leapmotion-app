import summarize from "app/data/calculate/summarize";

describe("summarize", function() {
    describe("when provided with values", function() {
        let results;
        beforeEach(function() {
            results = summarize({
                distanceDiffs: {X: [0, 1], Y: [0, 0], Z: [0, 0]},
                accelerations: {X: [0, -1], Y: [0, 0], Z: [0, 0]},
                jerks: {X: [0], Y: [0], Z: [0]},
                something: "else",
            });
        });

        it("sums up all elements, not caring about negatives", () => {
            expect(results).toEqual({
                trajectory: 1,
                acceleration: 1,
                jerk: 0,
                something: "else",
            });
        });
    });
});
