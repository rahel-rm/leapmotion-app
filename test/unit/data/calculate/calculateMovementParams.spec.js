import calculateMovementParams from "app/data/calculate/calculateMovementParams";

describe("calculateMovementParams", function() {
    describe("just two points", function() {
        let results;
        beforeEach(function() {
            const locations = {X: [0, 25], Y: [0, 75], Z: [0, 125]};
            const times = [0, 5];
            results = calculateMovementParams({locations, times});
        });

        it("calculates distance", function() {
            expect(results.distance.toPrecision(4)).toEqual("147.9");
        });

        it("calculates time difference", function() {
            expect(results.timeDiffs).toEqual([0, 5]);
        });

        it("calculates trajectory", function() {
            expect(results.distanceDiffs).toEqual({
                X: [0, 25],
                Y: [0, 75],
                Z: [0, 125],
            });
        });

        it("calculates speed", function() {
            expect(results.speeds).toEqual({
                X: [0, 5],
                Y: [0, 15],
                Z: [0, 25],
            });
        });

        it("calculates acceleration", function() {
            expect(results.accelerations).toEqual({
                X: [0, 1],
                Y: [0, 3],
                Z: [0, 5],
            });
        });

        it("calculates jerk", function() {
            expect(results.jerks).toEqual({
                X: [0, 0.2],
                Y: [0, 0.6],
                Z: [0, 1],
            });
        });
    });

    describe("uniform", function() {
        let results;
        beforeEach(function() {
            const locations = {X: [0, 25, 50], Y: [0, 50, 100], Z: [0, 0, 0]};
            const times = [0, 5, 10];
            results = calculateMovementParams({locations, times});
        });

        it("calculates distance", function() {
            expect(results.distance.toPrecision(4)).toEqual("111.8");
        });

        it("calculates time difference", function() {
            expect(results.timeDiffs).toEqual([0, 5, 5]);
        });

        it("calculates trajectory", function() {
            expect(results.distanceDiffs).toEqual({
                X: [0, 25, 25],
                Y: [0, 50, 50],
                Z: [0, 0, 0],
            });
        });

        it("calculates acceleration", function() {
            expect(results.accelerations).toEqual({
                X: [0, 1, 0],
                Y: [0, 2, 0],
                Z: [0, 0, 0],
            });
        });

        it("calculates jerk", function() {
            expect(results.jerks).toEqual({
                X: [0, 0.2, -0.2],
                Y: [0, 0.4, -0.4],
                Z: [0, 0, 0],
            });
        });
    });

    describe("accelerating", function() {
        let results;
        beforeEach(function() {
            const locations = {X: [0, 25, 75], Y: [0, 50, 100], Z: [0, 0, 0]};
            const times = [0, 5, 10];
            results = calculateMovementParams({locations, times});
        });

        it("calculates distance", function() {
            expect(results.distance).toEqual(125);
        });

        it("calculates time difference", function() {
            expect(results.timeDiffs).toEqual([0, 5, 5]);
        });

        it("calculates trajectory", function() {
            expect(results.distanceDiffs).toEqual({
                X: [0, 25, 50],
                Y: [0, 50, 50],
                Z: [0, 0, 0],
            });
        });

        it("calculates acceleration", function() {
            expect(results.accelerations).toEqual({
                X: [0, 1, 1],
                Y: [0, 2, 0],
                Z: [0, 0, 0],
            });
        });

        it("calculates jerk", function() {
            expect(results.jerks).toEqual({
                X: [0, 0.2, 0],
                Y: [0, 0.4, -0.4],
                Z: [0, 0, 0],
            });
        });
    });

    describe("goes back", function() {
        let results;
        beforeEach(function() {
            const locations = {X: [0, 50, 0], Y: [0, 100, 0], Z: [0, 0, 0]};
            const times = [0, 5, 10];
            results = calculateMovementParams({locations, times});
        });

        it("calculates distance", function() {
            expect(results.distance).toEqual(0);
        });

        it("calculates time difference", function() {
            expect(results.timeDiffs).toEqual([0, 5, 5]);
        });

        it("calculates trajectory", function() {
            expect(results.distanceDiffs).toEqual({
                X: [0, 50, -50],
                Y: [0, 100, -100],
                Z: [0, 0, 0],
            });
        });

        it("calculates acceleration", function() {
            expect(results.accelerations).toEqual({
                X: [0, 2, -4],
                Y: [0, 4, -8],
                Z: [0, 0, 0],
            });
        });

        it("calculates jerk", function() {
            expect(results.jerks).toEqual({
                X: [0, 0.4, -1.2],
                Y: [0, 0.8, -2.4],
                Z: [0, 0, 0],
            });
        });
    });

    describe("goes sideways", function() {
        let results;
        beforeEach(function() {
            const locations = {X: [0, 25, 25], Y: [0, 0, 50], Z: [0, 0, 0]};
            const times = [0, 5, 10];
            results = calculateMovementParams({locations, times});
        });

        it("calculates distance", function() {
            expect(results.distance.toPrecision(4)).toEqual("55.90");
        });

        it("calculates time difference", function() {
            expect(results.timeDiffs).toEqual([0, 5, 5]);
        });

        it("calculates trajectory", function() {
            expect(results.distanceDiffs).toEqual({
                X: [0, 25, 0],
                Y: [0, 0, 50],
                Z: [0, 0, 0],
            });
        });

        it("calculates acceleration", function() {
            expect(results.accelerations).toEqual({
                X: [0, 1, -1],
                Y: [0, 0, 2],
                Z: [0, 0, 0],
            });
        });

        it("calculates jerk", function() {
            expect(results.jerks).toEqual({
                X: [0, 0.2, -0.4],
                Y: [0, 0, 0.4],
                Z: [0, 0, 0],
            });
        });
    });
});
