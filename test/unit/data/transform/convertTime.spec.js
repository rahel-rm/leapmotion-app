import convertTime from "app/data/transform/convertTime";

describe("convertTime", function() {
    it("converts time into seconds", function() {
        expect(convertTime([
            {a: 1, b: "b", MicrosSinceStart: 0},
            {a: 1, b: "b", MicrosSinceStart: 5000000},
            {a: 1, b: "b", MicrosSinceStart: 12000000},
        ])).toEqual([
            {a: 1, b: "b", seconds: 0},
            {a: 1, b: "b", seconds: 5},
            {a: 1, b: "b", seconds: 12},
        ]);
    });
});
