import arrangeToAxes from "app/data/transform/arrangeToAxes";

describe("arrangeToAxes", function() {
    it("organises fields by axis", function() {
        const data = {
            fieldX: [0, 10, 40],
            fieldY: [0, 20, 60],
            fieldZ: [0, 30, 80],
            moreX: [0, 1, 4],
            moreY: [0, 2, 6],
            moreZ: [0, 3, 8],
            other: [1, 2, 3],
        };
        expect(arrangeToAxes(data, ["field", "more"])).toEqual({
            field: {
                X: [0, 10, 40],
                Y: [0, 20, 60],
                Z: [0, 30, 80],
            },
            more: {
                X: [0, 1, 4],
                Y: [0, 2, 6],
                Z: [0, 3, 8],
            },
            other: [1, 2, 3],
        });
    });
});
