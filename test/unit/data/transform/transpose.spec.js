import transpose from "app/data/transform/transpose";

describe("transpose", function() {
    it("turns list of objects into object with lists", function() {
        expect(transpose([
            {a: 1, b: "a", fieldX: 0, fieldY: 0, fieldZ: 0},
            {a: 2, b: "b", fieldX: 10, fieldY: 20, fieldZ: 30},
            {a: 3, b: "c", fieldX: 40, fieldY: 60, fieldZ: 80},
        ])).toEqual({
            a: [1, 2, 3],
            b: ["a", "b", "c"],
            fieldX: [0, 10, 40],
            fieldY: [0, 20, 60],
            fieldZ: [0, 30, 80],
        });
    });
});
