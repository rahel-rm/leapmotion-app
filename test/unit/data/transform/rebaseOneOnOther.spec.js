import rebaseOneOnOther from "app/data/transform/rebaseOneOnOther";

describe("rebaseOneOnOther", function() {
    it("bases target on base", function() {
        const data = [{
            baseX: 0,
            baseY: 10,
            baseZ: 20,
            targetX: 0,
            targetY: 15,
            targetZ: 45,
        }];

        expect(rebaseOneOnOther(data, "target", "base")).toEqual([{
            baseX: 0,
            baseY: 10,
            baseZ: 20,
            targetX: 0,
            targetY: 5,
            targetZ: 25,
        }]);
    });
});
