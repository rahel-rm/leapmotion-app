import transform from "app/data/transform";

describe("transform", function() {
    it("organises fields by axis", function() {
        const data = [{
            baseX: 0,
            baseY: 0,
            baseZ: 0,
            targetX: 0,
            targetY: 0,
            targetZ: 0,
            MicrosSinceStart: 1000000,
        }, {
            baseX: 10,
            baseY: 20,
            baseZ: 30,
            targetX: 20,
            targetY: 30,
            targetZ: 40,
            MicrosSinceStart: 2000000,
        }, {
            baseX: 40,
            baseY: 60,
            baseZ: 80,
            targetX: 60,
            targetY: 80,
            targetZ: 100,
            MicrosSinceStart: 3000000,
        }];
        const joints = ["base", "target"];
        const rebaseFields = {base: "base", target: "target"};

        expect(transform({data, joints, rebaseFields})).toEqual({
            base: {
                X: [0, 10, 40],
                Y: [0, 20, 60],
                Z: [0, 30, 80],
            },
            target: {
                X: [0, 10, 20],
                Y: [0, 10, 20],
                Z: [0, 10, 20],
            },
            seconds: [1, 2, 3],
        });
    });
});
