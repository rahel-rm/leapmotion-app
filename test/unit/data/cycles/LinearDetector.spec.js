import linearDetector from "app/data/cycles/LinearDetector";

describe("linearDetector", function() {
    beforeEach(() => {
        linearDetector.__set__({
            adjustCoordinates: {
                screenToLeapMotion: ({x}) => ([x - 700]),
                getPixelScale: () => 10,
            },
        });
    });

    it("reset", function() {
        linearDetector.reset();

        expect(linearDetector.getScreenCycleNr(-5)).toBe(0);
        expect(linearDetector.getScreenCycleNr(0)).toBe(0);
        expect(linearDetector.getScreenCycleNr(9)).toBe(0);
        expect(linearDetector.getScreenCycleNr(10)).toBe(0);
        expect(linearDetector.getScreenCycleNr(19)).toBe(0);
        expect(linearDetector.getScreenCycleNr(35)).toBe(0);
    });

    it("gets screen cycle nr", function() {
        linearDetector.setStartX(0);
        linearDetector.setPeriodWidth(10);

        expect(linearDetector.getScreenCycleNr(-5)).toBe(0);
        expect(linearDetector.getScreenCycleNr(0)).toBe(1);
        expect(linearDetector.getScreenCycleNr(9)).toBe(1);
        expect(linearDetector.getScreenCycleNr(10)).toBe(2);
        expect(linearDetector.getScreenCycleNr(19)).toBe(2);
        expect(linearDetector.getScreenCycleNr(35)).toBe(4);
    });

    it("gets leapmotion cycle nr when positive", function() {
        linearDetector.setStartX(700);
        linearDetector.setPeriodWidth(100);

        expect(linearDetector.getCycleNr(-5)).toBe(0);
        expect(linearDetector.getCycleNr(0)).toBe(1);
        expect(linearDetector.getCycleNr(9)).toBe(1);
        expect(linearDetector.getCycleNr(10)).toBe(2);
        expect(linearDetector.getCycleNr(19)).toBe(2);
        expect(linearDetector.getCycleNr(35)).toBe(4);
    });

    it("gets leapmotion cycle nr when negative", function() {
        linearDetector.setStartX(200); // 200 - 700 = -500
        linearDetector.setPeriodWidth(1000); // 1000 / 10 = 100

        // -500 -400 -300 -200 -100 0 100 200 300 400 500
        //     0    1    2    3    4 5   6   7   8   9

        expect(linearDetector.getCycleNr(-505)).toBe(0);
        expect(linearDetector.getCycleNr(-500)).toBe(1);
        expect(linearDetector.getCycleNr(-401)).toBe(1);
        expect(linearDetector.getCycleNr(-400)).toBe(2);
        expect(linearDetector.getCycleNr(-301)).toBe(2);
        expect(linearDetector.getCycleNr(-150)).toBe(4);
        expect(linearDetector.getCycleNr(150)).toBe(7);
        expect(linearDetector.getCycleNr(300)).toBe(9);
    });
});
