import cycles from "app/data/cycles";

describe("cycles", function() {
    beforeEach(function() {
        cycles.__set__({
            extractThresholds: (list) => list,
        });
    });

    it("breaks down to cycles", function() {
        const thresholdList = [0, 1];
        const dataByAxis = {
            base: {
                X: [0, 10, 40],
                Y: [0, 20, 60],
                Z: [0, 30, 80],
            },
            target: {
                X: [0, 10, 20],
                Y: [0, 10, 20],
                Z: [0, 10, 20],
            },
            seconds: [1, 2, 3],
        };

        expect(cycles({thresholdList, dataByAxis})).toEqual([{
            base: {
                X: [0],
                Y: [0],
                Z: [0],
            },
            target: {
                X: [0],
                Y: [0],
                Z: [0],
            },
            seconds: [1],
            time: 1,
            cycleNr: 0,
        }, {
            base: {
                X: [10, 40],
                Y: [20, 60],
                Z: [30, 80],
            },
            target: {
                X: [10, 20],
                Y: [10, 20],
                Z: [10, 20],
            },
            seconds: [2, 3],
            time: 1,
            cycleNr: 1,
        }]);
    });
});
