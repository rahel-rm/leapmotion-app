import cycleTime from "app/data/cycles/cycleTime";

describe("cycleTime", function() {
    describe("just two points", function() {
        it("breaks down to cycles", function() {
            const seconds = [0, 5];
            const thresholds = [0, 1];

            expect(cycleTime({seconds, thresholds})).toEqual([
                {cycleNr: 0, time: 5},
                {cycleNr: 1, time: 0},
            ]);
        });
    });

    describe("more points", function() {
        it("breaks down to cycles", function() {
            const seconds = [0, 2, 4, 6];
            const thresholds = [0, 2];

            expect(cycleTime({seconds, thresholds})).toEqual([
                {cycleNr: 0, time: 4},
                {cycleNr: 1, time: 2},
            ]);
        });
    });

    describe("two points with missing cycles inbetween", function() {
        it("breaks down to cycles, skipping the missing ones", function() {
            const seconds = [0, 10];
            const thresholds = [0, 1, 1, 1];

            expect(cycleTime({seconds, thresholds})).toEqual([
                {cycleNr: 0, time: 10},
                {cycleNr: 3, time: 0},
            ]);
        });
    });

    describe("two points in first cycle", function() {
        it("returns all in same cycle", function() {
            const seconds = [0, 5, 10];
            const thresholds = [0];

            expect(cycleTime({seconds, thresholds})).toEqual([
                {cycleNr: 0, time: 10},
            ]);
        });
    });

    describe("two points in same cycle", function() {
        it("returns all in same cycle", function() {
            const seconds = [0, 10, 15];
            const thresholds = [0, 0, 0, 1];

            expect(cycleTime({seconds, thresholds})).toEqual([
                {cycleNr: 2, time: 10},
                {cycleNr: 3, time: 5},
            ]);
        });
    });
});
