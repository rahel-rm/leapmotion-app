import arrangeDataToCycles from "app/data/cycles/arrangeDataToCycles";

describe("arrangeDataToCycles", function() {
    describe("just two points", function() {
        it("breaks down to cycles", function() {
            const cycles = [
                {cycleNr: 0, time: 1},
                {cycleNr: 1, time: 5},
            ];
            const dataByAxis = {
                base: {
                    X: [0, 10, 40],
                    Y: [0, 20, 60],
                    Z: [0, 30, 80],
                },
                target: {
                    X: [0, 10, 20],
                    Y: [0, 10, 20],
                    Z: [0, 10, 20],
                },
                seconds: [1, 2, 3],
            };
            const thresholds = [0, 1];

            expect(arrangeDataToCycles({cycles, dataByAxis, thresholds})).toEqual([{
                base: {
                    X: [0],
                    Y: [0],
                    Z: [0],
                },
                target: {
                    X: [0],
                    Y: [0],
                    Z: [0],
                },
                seconds: [1],
                time: 1,
                cycleNr: 0,
            }, {
                base: {
                    X: [10, 40],
                    Y: [20, 60],
                    Z: [30, 80],
                },
                target: {
                    X: [10, 20],
                    Y: [10, 20],
                    Z: [10, 20],
                },
                seconds: [2, 3],
                time: 5,
                cycleNr: 1,
            }]);
        });
    });

    describe("two points with missing cycles inbetween", function() {
        it("breaks down to cycles, skipping the missing ones", function() {
            const cycles = [
                {cycleNr: 0, time: 2},
                {cycleNr: 3, time: 5},
            ];
            const dataByAxis = {
                base: {
                    X: [0, 10, 40],
                    Y: [0, 20, 60],
                    Z: [0, 30, 80],
                },
                target: {
                    X: [0, 10, 20],
                    Y: [0, 10, 20],
                    Z: [0, 10, 20],
                },
                seconds: [1, 2, 3],
            };
            const thresholds = [0, 1, 1, 1];

            expect(arrangeDataToCycles({cycles, dataByAxis, thresholds})).toEqual([{
                base: {
                    X: [0],
                    Y: [0],
                    Z: [0],
                },
                target: {
                    X: [0],
                    Y: [0],
                    Z: [0],
                },
                seconds: [1],
                time: 2,
                cycleNr: 0,
            }, {
                base: {
                    X: [10, 40],
                    Y: [20, 60],
                    Z: [30, 80],
                },
                target: {
                    X: [10, 20],
                    Y: [10, 20],
                    Z: [10, 20],
                },
                seconds: [2, 3],
                time: 5,
                cycleNr: 3,
            }]);
        });
    });

    describe("two points in same cycle", function() {
        it("returns all in same cycle", function() {
            const cycles = [
                {cycleNr: 1, time: 6},
            ];
            const dataByAxis = {
                base: {
                    X: [0, 10, 40],
                    Y: [0, 20, 60],
                    Z: [0, 30, 80],
                },
                target: {
                    X: [0, 10, 20],
                    Y: [0, 10, 20],
                    Z: [0, 10, 20],
                },
                seconds: [1, 2, 3],
            };
            const thresholds = [0, 0];

            expect(arrangeDataToCycles({cycles, dataByAxis, thresholds})).toEqual([{
                base: {
                    X: [0, 10, 40],
                    Y: [0, 20, 60],
                    Z: [0, 30, 80],
                },
                target: {
                    X: [0, 10, 20],
                    Y: [0, 10, 20],
                    Z: [0, 10, 20],
                },
                seconds: [1, 2, 3],
                time: 6,
                cycleNr: 1,
            }]);
        });
    });
});
