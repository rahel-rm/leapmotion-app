import extractThresholds from "app/data/cycles/extractThresholds";

describe("extractThresholds", function() {
    beforeEach(function() {
        extractThresholds.__set__({
            linearDetector: {
                getCycleNr: (x) => Math.floor(x / 10),
            },
        });
    });

    it("includes negatives in cycle 0", function() {
        expect(extractThresholds([-10, -5, 0, 5])).toEqual([0]);
    });

    it("returns index of each new cycle", function() {
        expect(extractThresholds([-10, 10, 20])).toEqual([0, 1, 2]);
    });

    it("does not go back a cycle when value drops too much", function() {
        expect(extractThresholds([10, 0])).toEqual([0, 0]);
        expect(extractThresholds([10, 0, 20, 0])).toEqual([0, 0, 2]);
    });

    it("sets cycle start equal to previous when there is nothing there", function() {
        expect(extractThresholds([20, 0])).toEqual([0, 0, 0]);
        expect(extractThresholds([40, 0])).toEqual([0, 0, 0, 0, 0]);
    });
});
