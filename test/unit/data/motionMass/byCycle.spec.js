import byCycle from "app/data/motionMass/byCycle";

describe("byCycle", function() {
    beforeEach(() => {
        byCycle.__set__({
            cycles: () => ([{
                base: {X: [0], Y: [0], Z: [0]},
                target: {X: [0], Y: [0], Z: [0]},
                seconds: [1],
                time: 1,
                cycleNr: 0,
            }, {
                base: {
                    X: [10, 40],
                    Y: [20, 60],
                    Z: [30, 80],
                },
                target: {
                    X: [10, 20],
                    Y: [10, 20],
                    Z: [10, 20],
                },
                seconds: [2, 4],
                time: 1,
                cycleNr: 1,
            }]),

        });
    });

    it("gets motionMass for joint and calls cycles", function() {
        const dataByAxis = {
            base: {
                X: [0, 10, 40],
                Y: [0, 20, 60],
                Z: [0, 30, 80]},
            target: {
                X: [0, 10, 20],
                Y: [0, 10, 20],
                Z: [0, 10, 20]},
            seconds: [1, 2, 3],
        };
        const xPositions = [0, 1];
        const joint = "base";

        expect(byCycle({dataByAxis, xPositions, joint})).toMatchSnapshot();
    });
});
