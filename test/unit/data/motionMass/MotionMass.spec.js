import MotionMass from "app/data/motionMass/MotionMass";

describe("MotionMass", function() {
    beforeEach(() => {
        MotionMass.__set__({
            saveToFile: () => {},
        });
    });

    describe("with data", function() {
        it("sorts by time, transforms and sends to byJoint", function() {
            const data = [{
                FrameNr: 3,
                MicrosSinceStart: 2000000,
                IndexTipPositionX: -208,
                IndexTipPositionY: 215,
                IndexTipPositionZ: 97,
                IndexTipVelocityX: 13,
                IndexTipVelocityY: 7,
                IndexTipVelocityZ: -15,
                WristPositionX: -190,
                WristPositionY: 141,
                WristPositionZ: 221,
            }, {
                FrameNr: 0,
                MicrosSinceStart: 0,
                IndexTipPositionX: -208,
                IndexTipPositionY: 215,
                IndexTipPositionZ: 97,
                IndexTipVelocityX: 30,
                IndexTipVelocityY: 19,
                IndexTipVelocityZ: -4,
                WristPositionX: -190,
                WristPositionY: 141,
                WristPositionZ: 221,

            }];
            const motionMass = new MotionMass(data, ["IndexTipPosition", "WristPosition"]);
            motionMass.calc();

            expect(motionMass.getData()).toMatchSnapshot();
        });
    });

    describe("with empty data", function() {
        it("returns nothing", function() {
            const motionMass = new MotionMass([], ["wrist"]);
            motionMass.calc();

            expect(motionMass.getData()).toEqual([]);
        });
    });
});
