import byJoint from "app/data/motionMass/byJoint";

describe("byJoint", function() {
    beforeEach(() => {
        byJoint.__set__({
            calculateMovementParams: () => ({a: 1, b: 2}),
            summarize: (data) => ({...data, sum: true}),
            byCycle: () => (["cycle"]),
        });
    });

    it("gets motionMass for joint and calls cycles", function() {
        const dataByAxis = {
            WristPosition: {X: [0, 25], Y: [0, 75], Z: [0, 125]},
            IndexTipPosition: {X: [0, 2], Y: [0, 7], Z: [0, 12]},
        };
        const xPositions = [];
        const joints = ["WristPosition", "IndexTipPosition"];

        expect(byJoint({dataByAxis, xPositions, joints})).toMatchSnapshot();
    });
});
