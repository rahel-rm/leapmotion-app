import extractFields from "app/data/extract/extractFields";

describe("extractFields", function() {
    it("organises fields by axis", function() {
        const data = [{
            GrabStrength: 1, PinchStrength: 0.9,
            IndexTipVelocityX: 2, IndexTipVelocityY: 3, IndexTipVelocityZ: 0,
            fieldX: 4, fieldY: 5, fieldZ: 6,
            moreX: 14, moreY: 15, moreZ: 16,
            some: 1, extra: 2, nonsense: 3,
        },
        {
            GrabStrength: 0.5, PinchStrength: 0.4,
            IndexTipVelocityX: 3, IndexTipVelocityY: 3, IndexTipVelocityZ: 0,
            fieldX: 6, fieldY: 8, fieldZ: 10,
            moreX: 15, moreY: 16, moreZ: 17,
        }];

        expect(extractFields({data, joints: ["field", "more"]})).toEqual({
            grabStrength: [1, 0.5], pinchStrength: [0.9, 0.4],
            leapVelocityX: [2, 3], leapVelocityY: [3, 3], leapVelocityZ: [0, 0],
            leapVelocityEucl: [0, 1],
            fieldX: [4, 6], fieldY: [5, 8], fieldZ: [6, 10],
            fieldXDelta: [0, 2], fieldYDelta: [0, 3], fieldZDelta: [0, 4],
            moreX: [14, 15], moreY: [15, 16], moreZ: [16, 17],
            moreXDelta: [0, 1], moreYDelta: [0, 1], moreZDelta: [0, 1],
        });
    });
});
