import parseFrame from "app/data/extract/parseFrame.js";
import initialData from "./testData.js";

describe("parseFrame", function() {
    it("returns nothing when frame is empty", function() {
        expect(parseFrame()).toBe(undefined);
    });

    it.skip("returns parsed data", function() {
        const metadata = {MicrosSinceStart: 2, subjectCode: 1};
        expect(parseFrame(initialData, metadata)).toMatchSnapshot();
    });

    it("ignore invalid data", function() {
        expect(
            parseFrame({
                valid: false,
            })
        ).toBe(undefined);
    });
});
