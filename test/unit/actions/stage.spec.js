import * as actions from "app/actions/stage";

describe("actions", () => {
    it("should onGoToPage should create onGoToPage action", () => {
        expect(actions.onGoToPage("page")).toMatchSnapshot();
    });

    it("should onStartTest should create onStartTest action", () => {
        expect(actions.onStartTest("someTest")).toMatchSnapshot();
    });

    it("should onIncrementCount should create onIncrementCount action", () => {
        expect(actions.onIncrementCount()).toMatchSnapshot();
    });

    it("should onStartTestTimer should create onStartTestTimer action", () => {
        expect(actions.onStartTestTimer()).toMatchSnapshot();
    });
});
