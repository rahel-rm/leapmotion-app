import * as actions from "app/actions/subject";

describe("actions", () => {
    it("should onChangeSubjectCode should create onChangeSubjectCode action", () => {
        expect(actions.onChangeSubjectCode("PG")).toMatchSnapshot();
    });

    it("should onSaveSubjectCode should create onSaveSubjectCode action", () => {
        expect(actions.onSaveSubjectCode()).toMatchSnapshot();
    });
});
