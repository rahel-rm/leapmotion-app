import subject from "app/reducers/subject";
import {ON_CHANGE_SUBJECT_CODE} from "app/actions/subject";

describe("reducers", () => {
    describe("subject", () => {
        it("should handle initial state", () => {
            expect(subject(undefined, {})).toMatchSnapshot();
        });

        it("should handle ON_CHANGE_SUBJECT_CODE", () => {
            expect(subject("a", {type: ON_CHANGE_SUBJECT_CODE})).toMatchSnapshot();
        });

        it("should handle unknown action type", () => {
            expect(subject("a", {type: "unknown"})).toMatchSnapshot();
        });
    });
});
