import stage from "app/reducers/stage";
import {ON_GO_TO_PAGE, ON_START_TEST, ON_INCREMENT_COUNT, ON_START_TEST_TIMER} from "app/actions/stage";

describe("reducers", () => {
    describe("stage", () => {
        it("should handle initial state", () => {
            expect(stage(undefined, {current: "home"})).toMatchSnapshot();
        });

        it("should handle ON_GO_TO_PAGE", () => {
            expect(stage({}, {type: ON_GO_TO_PAGE, nextPage: "a page"})).toMatchSnapshot();
        });

        it("should handle ON_START_TEST", () => {
            expect(stage({}, {type: ON_START_TEST, test: "some test"})).toMatchSnapshot();
        });

        it("should handle ON_INCREMENT_COUNT", () => {
            expect(stage({count: 2}, {type: ON_INCREMENT_COUNT})).toMatchSnapshot();
        });

        it("should handle ON_START_TEST_TIMER", () => {
            const _now = Date.now;
            Date.now = jest.fn(() => 123);
            try {
                expect(stage({}, {type: ON_START_TEST_TIMER})).toMatchSnapshot();
            }
            finally {
                Date.now = _now; // restore original now meth. on object
            }
        });

        it("should handle unknown action type", () => {
            expect(stage({}, {type: "unknown"})).toMatchSnapshot();
        });
    });
});
