/* eslint no-console: 0 */

import {Application} from "spectron";
import electronPath from "electron";
import path from "path";

jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;

const delay = time => new Promise(resolve => setTimeout(resolve, time));

describe.skip("main window", function spec() {
    beforeAll(async() => {
        this.app = new Application({
            path: electronPath,
            args: [path.join(__dirname, "..", "..", "app")],
        });

        return this.app.start();
    });

    afterAll(async() => {
        if (this.app && this.app.isRunning()) {
            // await delay(10000);
            return this.app.stop();
        }
    });

    it("should open window", async() => {
        const {client, browserWindow} = this.app;

        await client.waitUntilWindowLoaded();
        await delay(500);
        const title = await browserWindow.getTitle();
        expect(title).toBe("LeapMotion Application");
    });

    it("should go to menu with click on \"Alusta\" link", async() => {
        const {client} = this.app;

        console.log(await this.app.client.element("[data-tid=\"continue\"]"));
        console.log(await this.app.client.element("[data-tid=\"menu-header\"]"));
        await client.click("[data-tid=\"continue\"]");
        const findHeader = () => this.app.client.element("[data-tid=\"menu-header\"]");
        // console.log(findHeader().getText());
        expect(await findHeader().getText()).toBe("Harjutused");
    });
});
